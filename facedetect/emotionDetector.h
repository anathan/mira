#ifndef EMOTION_DETECTION_H_
#define EMOTION_DETECTION_H_

#include <opencv2/opencv_modules.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include "opencv2/nonfree/nonfree.hpp"

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/timer/timer.hpp>

class EmotionDetector
{

public:

    cv::CascadeClassifier cascadeFace;
    cv::CascadeClassifier cascadeEyesA;
    cv::CascadeClassifier cascadeEyesB;
    cv::CascadeClassifier cascadeMouth;
    cv::CascadeClassifier cascadeSmile;
    cv::CascadeClassifier cascadeNose;
    cv::CascadeClassifier cascadeUpperBody;

    EmotionDetector();
    ~EmotionDetector();
};

#endif
