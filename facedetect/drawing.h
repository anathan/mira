#include "opencv2/imgproc/imgproc.hpp"

//Converts a point in the image to a desired yaw/pitch assuming purely linear scaling
cv::Point static frameToYP(cv::Point imgPnt, cv::Mat& img, int YAW_RANGE, int YAW_CENTER, int PITCH_RANGE, int PITCH_CENTER)
{
    int width = img.cols;
    int height = img.rows;
    //move/flip the image coordinates so the centerpoint is at 0,0
    //we're flipping the y axis too..so y positive is now up in the image
    //and then scaling this to be -0.5/0.5
    cv::Point2f imgPntNormalized((imgPnt.x - width / 2) / (float)width, (height / 2 - imgPnt.y) / (float)height);

    //now map the x offset to yaw and the y offset to pitch
    //NB: this is geometrically just an approximation. hwowever, we're targeting the center point
    //which this approximation is perfect for.

    int yaw = (int)((-YAW_RANGE * imgPntNormalized.x) + YAW_CENTER);
    int pitch = (int)((PITCH_RANGE * imgPntNormalized.y) + PITCH_CENTER);

    return cv::Point(yaw, pitch);
}

cv::Point static ypToYpImg(double yawValue, double pitchValue, int YAW_RANGE, int YAW_MIN, int PITCH_RANGE, int PITCH_MIN)
{
    //simple transform to take a YAW_CENTER,PITCH_CENTER centered and draw as it should be projected on to the cvMat YPR image
    return cv::Point((int)(yawValue - YAW_MIN), (int)(PITCH_RANGE - (pitchValue - PITCH_MIN)));
}

void static drawString(cv::Mat& img, cv::Point offset, const std::string text)
{
    cv::Size size = cv::getTextSize(text, cv::FONT_HERSHEY_PLAIN, 1.0f, 1, NULL);
    putText(img, text, cv::Point(offset.x + 0, offset.y + size.height), cv::FONT_HERSHEY_PLAIN, 1.0f, cv::Scalar(255, 255, 255));

}

void static drawCross(CvPoint pt, cv::Mat& img, cv::Scalar s)
{
    int size = 10;
    cv::line(img, cv::Point(pt.x, pt.y - size), cv::Point(pt.x, pt.y + size), s, 2);
    cv::line(img, cv::Point(pt.x - size, pt.y), cv::Point(pt.x + size, pt.y), s, 2);
}