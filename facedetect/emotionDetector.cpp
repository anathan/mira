#include "emotionDetector.h"

using namespace std;
using namespace cv;

EmotionDetector::EmotionDetector()
{
#ifdef _MSC_VER
    
    cascadeFace.load("C:/data/lbpcascades/lbpcascade_frontalface.xml");
    cascadeEyesA.load("C:/data/haarcascades/haarcascade_mcs_eyepair_small.xml");
    cascadeEyesB.load("C:/data/haarcascades/haarcascade_mcs_eyepair_big.xml");
    cascadeNose.load("C:/data/haarcascades/haarcascade_mcs_nose.xml");
    cascadeMouth.load("C:/data/haarcascades/haarcascade_mcs_mouth.xml");
    cascadeUpperBody.load("C:/data/haarcascades/haarcascade_mcs_upperbody.xml");
#else
    cascadeFace.load("/home/pi/data/lbpcascades/lbpcascade_frontalface.xml");    
    cascadeEyesA.load("/home/pi/data/haarcascades/haarcascade_mcs_eyepair_small.xml");
    cascadeEyesB.load("/home/pi/data/haarcascades/haarcascade_mcs_eyepair_big.xml");
    cascadeNose.load("/home/pi/data/haarcascades/haarcascade_mcs_nose.xml");
    cascadeMouth.load("/home/pi/data/haarcascades/haarcascade_mcs_mouth.xml");
    cascadeUpperBody.load("/home/pi/data/haarcascades/haarcascade_mcs_upperbody.xml");
#endif
}


EmotionDetector::~EmotionDetector()
{
}
