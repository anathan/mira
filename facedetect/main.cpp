#include <iostream>
#include <fstream>

#include <boost/regex.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "drawing.h"
#include "emotionDetector.h"
//#include "sharedmemory.h"

#include "../common/network/sender.h"
#include "../common/network/receiver.h"
#include "../common/network/endpoints.h"

#include "../common/interfaces/controller.h"
#include "../common/interfaces/faces.h"

#if !defined(_MSC_VER) && !defined(__APPLE__) 
    #include <raspicam/raspicam_cv.h>
    #include <time.h>
#endif

using namespace std;
using namespace cv;

Sender* faceSender;
Sender* ctrlSender;
static int uiScaleFactor = 2;
static int yawEffortInt = 11;
static int pitchEffortInt = 6;
static int deadZone = 4;

double fdDt=0;
double cycleDt=0;

bool gui = true;
Mat ui;
Mat frame;

Point imgTarget = Point(320, 240);

int modPrintJson=0;
void sendFaces(vector<Rect>& faces)
{
    std::string json = toJson(faces);
    if (!gui && ++modPrintJson%10==0)
    {	
        std::cout<< "dt" << std::setw(7) << std::setprecision(3) <<  fdDt <<" F: " << json << std::endl;
        cv::imwrite("raw.jpg",frame);
    }
    faceSender->send(json);
}

void sendControl(int yawValue, int pitchValue, int rollValue)
{    
    ctrlSender->send(toJson(yawValue, pitchValue, rollValue));
}
 
int main(int argc, char** argv){

    try 
    {
        boost::program_options::options_description desc("Allowed options");
        desc.add_options()
            ("help", "produce help message")
            ("buildinfo", "shows the build info for the app")
            ("gui", boost::program_options::value<bool>()->default_value(true), "false hides the gui")            
            ;

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
        boost::program_options::notify(vm);
        if (vm.count("help"))
        {
            cout << desc << endl;
            return 0;
        }
        if (vm.count("buildinfo")) 
        {
            cout << "BUILD INFO " << endl << getBuildInformation() << endl;
            cout << "THREAD INFO " << endl << getNumThreads() << endl;
            cout << "OPTIMIZED " << endl << useOptimized() << endl;            
            return 0;
        }
        if (vm.count("gui"))
        {
            cout << "Gui is set to " << vm["gui"].as<bool>() << endl;
            gui = vm["gui"].as<bool>();            
        }
    }
    catch (exception& e) {
        cerr << "Error parsing command line options: " << e.what() << "\n";
        return 1;
    }
   
    boost::asio::io_service io_service;  
    faceSender = new Sender(io_service, LISTEN_IP_AUTO, FACE_DETECT_IP, FACE_DETECT_PORT);
    ctrlSender = new Sender(io_service, LISTEN_IP_AUTO, CONTROLLER_IP, CONTROLLER_PORT);    
    boost::thread t(boost::bind(&boost::asio::io_service::run, &io_service));

    EmotionDetector emoDetector;
	bool cameraFound = true;

#if defined( _MSC_VER) || defined(__APPLE__)
    VideoCapture cap;
    cap.set(CV_CAP_PROP_FORMAT, CV_8UC1);
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
    cap.set(CV_CAP_PROP_BRIGHTNESS, 50);
    cap.set(CV_CAP_PROP_CONTRAST, 50);
    cap.set(CV_CAP_PROP_SATURATION, 50);
    cap.set(CV_CAP_PROP_GAIN, 50);
	cap.set(CV_CAP_PROP_FPS, 30);
    cap.open(CV_CAP_DSHOW + 0);
    if (!cap.isOpened())
    {
        cout << "Cannot open camera" << endl;
		cameraFound = false;
    }  
#else
    raspicam::RaspiCam_Cv cap;
    cap.set(CV_CAP_PROP_FORMAT, CV_8UC1);
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
    cap.set(CV_CAP_PROP_BRIGHTNESS, 50);
    cap.set(CV_CAP_PROP_CONTRAST, 50);
    cap.set(CV_CAP_PROP_SATURATION, 50);
    cap.set(CV_CAP_PROP_GAIN, 50);

    cout << "Connecting to camera" << endl;
    if (!cap.open()) {
        cerr << "Error opening camera" << endl;
		cameraFound = false;
    }
    cout << "Connected to camera =" << cap.getId() << endl;
#endif

    if (gui)
    {
        namedWindow("cam", CV_WINDOW_AUTOSIZE);       
    }    
 
	//SharedMemory mem("cammap", 640*480*3); 
	//mem.CreateNewEvent(nullptr, true, false, "ImageReplySignal");

    double startCycle = (double)getTickCount();
    //------------------------------------------------------------------------------
    while (1)
    {

#ifdef _MSC_VER
		if (cameraFound)
			cap.read(frame);
		if (frame.rows == 0)
		{
			cvWaitKey(10);
			continue;
		}
#else
		if (cameraFound)
		{
			cap.grab();
			cap.retrieve(frame);
		}
        flip(frame, frame, 0);
#endif
		

		//shared memeory/
		//unsigned char* memPtr = static_cast<unsigned char*>(mem.GetDataPointer());
		
		//frame.data

		///-----
		Mat frameSmall;
        resize(frame, frameSmall, Size(320,240));
        ui = Mat(320 * uiScaleFactor, 240 * uiScaleFactor, CV_8UC3);
        resize(frame, ui, Size(320 * uiScaleFactor, 240 * uiScaleFactor));

        GaussianBlur(frameSmall, frameSmall, Size(3, 3), 1);
        
        vector<Rect> faces;
        double startFaceDetect = (double)getTickCount();
        emoDetector.cascadeFace.detectMultiScale(frameSmall, faces, 1.1, 3, CV_HAAR_SCALE_IMAGE | CV_HAAR_FEATURE_MAX, Size(60,60));
        double faceDetectTime = (double)getTickCount() - startFaceDetect;

        //Send the face messages on the wire
        sendFaces(faces);

	   
        //draw the ypr frame
        fdDt = faceDetectTime / getTickFrequency();
        cycleDt = (getTickCount() - startCycle) / getTickFrequency();
        startCycle = getTickCount();
        if (gui)
        {
			Point2d ctrFace(320, 240);
			Point2d center(320, 240);
			double closestToCenter = 1000;
			//get the center coordinate of the "first" face
			for (unsigned int i = 0; i < faces.size(); i++)
			{
				Point tlSmall = faces[i].tl();
				Point brSmall = faces[i].br();

				Point2d tl = Point2d(tlSmall.x * uiScaleFactor, tlSmall.y * uiScaleFactor);
				Point2d br = Point2d(brSmall.x * uiScaleFactor, brSmall.y * uiScaleFactor);
				Point2d ctr = Point2d((tl.x + br.x) / 2, (tl.y + br.y) / 2);

				//track the face closest to the center
				double distToCenter = cv::norm(ctr - center);
				if (distToCenter < closestToCenter)
				{
					ctrFace = ctr;
					closestToCenter = distToCenter;
					if (gui)
						rectangle(ui, tl, br, CV_RGB(255, 255, 255), 2);
				}
			}

			drawCross(ctrFace, ui, CV_RGB(0, 255, 0));
			drawCross(center, ui, CV_RGB(255, 255, 255));
			line(ui, center, ctrFace, CV_RGB(255, 0, 0), 2);
		    drawString(ui, Point(310, 10), (boost::format("Faces %d R %5fhz FD: %5fms") % faces.size() % (1.0 / cycleDt) % (fdDt * 1000)).str());
            imshow("cam", ui);
        }
        

        if (gui && waitKey(10) == 27)
        {
           break;
        } 
        else
        {
#if defined( _MSC_VER) || defined(__APPLE__)
            boost::this_thread::sleep(boost::posix_time::milliseconds(1));
#else
            nanosleep((struct timespec[]){ {0, 1000000} }, NULL); //1ms
#endif
        }
    }
    std::cout << "Exiting" << std::endl;
    io_service.stop();
    t.join();
  
    return 0;
}
