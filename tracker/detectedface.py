import math

class DetectedFace(object):
    def __init__(self, face, id): #NB: face is (x, y, w, h)
        self.face = face
        self.detections = 0
        self.misses = 0
        self.id = id
        self.tracking = False
        self.first_detection_time = None
        self.last_miss_time = None
    
    def scale_diff(self, other_face):
        return math.sqrt((self.face[2] - other_face.face[2])**2 + (self.face[3] - other_face.face[3])**2)

    def distance(self, other_face):
        (x, y, w, h) = self.face
        (x1,y1) = (x+w/2, y + h/2)
        (x, y, w, h) = other_face.face
        (x2,y2) = (x+w/2, y + h/2)
        return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

    def overlap(self,other_face):
        (r1left, r1top, sw, sh) = self.face
        r1right = r1left + sw
        r1bottom = r1top + sh
        (r2left, r2top, ow, oh) = other_face.face
        r2right = r2left + ow
        r2bottom = r2top + oh
        
        c1 = r1left < r2right and r1right > r2left 
        c2 = r1top < r2bottom and r1bottom > r2top 
        return c1 and c2