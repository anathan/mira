import sys
sys.path.append('../common')
sys.path.append('../common/network')
import math, json, time, random, copy, platform

import cv2 
import numpy as np
import util
import cmt
import multiprocessing
import camera
import tsconstants
import tsnetwork
from numpy import empty, nan
from detectedface import DetectedFace
from multiprocessing.pool import ThreadPool
from multiprocessing import Process, Pipe, Value, Lock
from collections import deque
from tsconstants import *
from tscommon import Point, Face, scale

SCALE = 2
size = 240 * SCALE, 320 * SCALE, 3
frame = np.zeros(size, dtype=np.uint8)
faces = []
cv2.putText(frame,'Waiting for Image...', (20,150), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,0), 1)
       

def on_image(msg):
    global frame
    nparr = np.fromstring(msg, np.uint8)
    raw_frame = cv2.imdecode(nparr, cv2.CV_LOAD_IMAGE_COLOR)
    frame = cv2.resize(raw_frame, (0,0), fx=2.0, fy=2.0) 
   
def on_faces(msg):
    global faces
    faces = []
    j = json.loads(msg)
            
    for fj in j["faces"]:
        face = Face(
            Point(fj["topleft"]["x"],fj["topleft"]["y"]),
            Point(fj["bottomright"]["x"],fj["bottomright"]["y"]))
        face.first_detection_time = fj["fdt"]
        face.last_miss_time = fj["lmt"]
        face.id = fj["id"]
        face.tracked = fj["tracked"]
        faces.append(face)



if __name__ == '__main__':  
    rxMgr  = tsnetwork.UdpManager()
    rxMgr.register(tsnetwork.UdpReceiver(LISTEN_IP, CAMERA_PREVIEW_IP, CAMERA_PREVIEW_PORT, on_image))
    rxMgr.register(tsnetwork.UdpReceiver(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT, on_faces))

    while True:
        rxMgr.loop()
        gui = frame.copy()
        for f in faces:                
            if f.first_detection_time < 0.2:
                cv2.rectangle(gui, scale(f.tl_cv(),SCALE), scale(f.br_cv(),SCALE), (0, 0, 255), 2)
            else:
                cv2.rectangle(gui, scale(f.tl_cv(),SCALE), scale(f.br_cv(),SCALE), (0, 255, 0), 2)
            dt = 0 if f.first_detection_time is None else f.first_detection_time
            mt = 0 if f.last_miss_time is None else f.last_miss_time
            cv2.putText(gui,str(f.id) + "d: {0:1.1f} m: {1:1.1f}".format(dt,mt), scale(f.tl_cv(),2), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,0), 1)
        cv2.imshow('Remote Video', gui)
        if cv2.waitKey(30) & 0xFF == ord('q'):
           break