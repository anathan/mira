import sys
sys.path.append('../common')
sys.path.append('../common/network')
import math, json, time, random, copy, platform

import cv2 
import numpy as np
import util
import cmt
import multiprocessing
import camera
import tsconstants
import tsnetwork
from numpy import empty, nan
from detectedface import DetectedFace
from multiprocessing.pool import ThreadPool
from multiprocessing import Process, Pipe, Value, Lock
from collections import deque
from tsconstants import *

####################configuration###################
show_gui = False# True
network_preview = True
save = False #True
estimate_rotation = True
estimate_scale = True
enable_tracking = False
print_json = True
print_timing = False
MISS_TIME_THRESHOLD = 0.5    #seconds
DETECT_TIME_THRESHOLD = 0.2
TRACK_LOST_COUNT_THRESHOLD = 3
#####################################################


#default for windows paths....others are specified below
casc_face_path = 'c:/data/haarcascades/haarcascade_frontalface_alt2.xml'
casc_face_feature_path = 'c:/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml' 
if platform.system() == "Windows":
    pass   
elif platform.system() == "Darwin":    
    casc_face_path = 'haarcascade_frontalface_alt2.xml'
    casc_face_feature_path = 'haarcascade_eye_tree_eyeglasses.xml'    
else:
    casc_face_path = '/home/pi/data/haarcascades/haarcascade_frontalface_alt2.xml'
    casc_face_feature_path = '/home/pi/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml' 



def track_faces(gray):
    if tracking:
        cmt.process_frame(gray)

def init_tracker(cmt, tracking, track_lost_count, associated_faces):
    if tracking and not cmt.has_result: 
        track_lost_count += 1
        if track_lost_count > TRACK_LOST_COUNT_THRESHOLD:
            track_lost_count=0
            print '###CMT LOST###'
            tracking = False

    #HACK: for now use the first face that exists. eventually, we'll spawn a tracker for each face up to N
    if len (associated_faces) > 0 and not tracking :
        f = associated_faces[0]
        if time.time() - f.first_detection_time > DETECT_TIME_THRESHOLD:
            (x,y,w,h) = f.face
            padw = w/12
            padh = h/12
            tl = (x+padw,y+padh)
            br = (x + w-(padw*2), y + h)
            if cmt.initialise(gray, tl, br):
                print '###CMT INIT####'
                tracking = True
            else:
                print 'CMT failed to initialize. Likely not enough keypoints'
        else:
            print 'attempting to init but dt is too short'   
    
    return tracking, track_lost_count

def associate_faces(faces, prev_tracked_faces):
    # attempt to "track" the faces between frames 
    #first try to associate the faces to ones we already have...
    # strategy is just to see if it overlaps, and take the first one that does
    new_tracked_faces = []
    birthed_faces = []
    for f in faces:
        new_face = DetectedFace(f, -1)       
        prev_face_to_remove = None
        for prev_face in prev_tracked_faces:
           if prev_face.overlap(new_face):
               #this is a positive redetection
               #increment detection count, copy the id and clear the misses (and maybe figure out how to copy construct in python?)
               new_face = copy.copy(prev_face)
               new_face.detections += 1               
               new_face.face = f               
               new_face.last_miss_time = None               
               prev_face_to_remove = prev_face 
               new_tracked_faces.append(new_face)
               break
        if prev_face_to_remove == None:
            new_face.id = random.randint(1,100000000)
            new_face.detections +=  1       
            new_face.first_detection_time = time.time()       
            new_face.misses = 0               
            new_face.last_miss_time = None
            new_tracked_faces.append(new_face)
            birthed_faces.append(new_face)
        else:
            prev_tracked_faces.remove(prev_face_to_remove)

    
    #now the remaining faces were "lost", increment their misses
    for f in prev_tracked_faces:        
        f.misses += 1    
        if f.last_miss_time is None:
            f.last_miss_time = time.time()           
        # only keep tracking things that are below the miss threshold AND have been tracked over the detect time threshold
        # this makes sure newly created targets have to be steady before they are kepped around.
        if time.time() - f.last_miss_time < MISS_TIME_THRESHOLD and time.time() - f.first_detection_time > DETECT_TIME_THRESHOLD:
            new_tracked_faces.append(f)

    return (new_tracked_faces, birthed_faces)

def track(gray, cmt):
    
    #update the tracking    
    if enable_tracking and tracking:
        cmt.process_frame(gray)
    
              
def gui(frame, associated_faces):
    for f in associated_faces:                
        (x,y,w,h) = f.face            
        
        if time.time() - f.first_detection_time < DETECT_TIME_THRESHOLD:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)
        else:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        dt = 0 if f.first_detection_time is None else time.time() - f.first_detection_time
        mt = 0 if f.last_miss_time is None else time.time() - f.last_miss_time
        
        
        cv2.putText(frame,str(f.id) + "d: {0:1.1f} m: {1:1.1f}".format(dt,mt), (x+5,y+h-5), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,0), 1)
        '''
        
        if cmt.has_result:
            kpsum = len(cmt.active_keypoints) + len(cmt.tracked_keypoints)
            cv2.line(frame, cmt.tl, cmt.tr, (255, 0, 0), 4)
            cv2.line(frame, cmt.tr, cmt.br, (255, 0, 0), 4)
            cv2.line(frame, cmt.br, cmt.bl, (255, 0, 0), 4)
            cv2.line(frame, cmt.bl, cmt.tl, (255, 0, 0), 4)
    
            util.draw_keypoints(cmt.tracked_keypoints, frame, (255, 255, 255))

            # this is from simplescale
            util.draw_keypoints(cmt.votes[:, :2], frame)  # blue
            util.draw_keypoints(cmt.outliers[:, :2], frame, (0, 0, 255))

            if not math.isnan(cmt.center[0]) and not math.isnan(cmt.center[1]):
                cv2.circle(frame, 
                        (int( cmt.center[0]), int( cmt.center[1])),
                        3, 
                        (0,255,0))
         '''
    cv2.imshow('Video', frame)

        
def find_faces(gray,classifier):
    #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    height, width = gray.shape
    t = time.time()
    detections = classifier.detectMultiScale(
        image=gray,        
        scaleFactor=1.2,
        minNeighbors=3,
        minSize=(width/7, height/7),
        #minSize=(60,60),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE | cv2.cv.CV_HAAR_FEATURE_MAX
    )    
   # print 'fdt', (time.time() - t)    
    return detections

def detect_worker(pipe,processing_flag):
    output_p, input_p = pipe
    prev_associated_faces = []
    prev_t = time.time()
    face_cascade = cv2.CascadeClassifier(casc_face_path)
    face_feature_cascade = cv2.CascadeClassifier(casc_face_feature_path)
    tx = tsnetwork.UdpSender(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT)    
    tx_preview = tsnetwork.UdpSender(LISTEN_IP, CAMERA_PREVIEW_IP, CAMERA_PREVIEW_PORT)
    frame_count = 0
    while True:
        frame = output_p.recv()   
        processing_flag.value = 0
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = find_faces(gray, face_cascade)
        associated_faces, birthed_faces = associate_faces(faces, prev_associated_faces)        
        util.send_results(associated_faces, None, tx, print_json) 
        #print len(associated_faces)
        if frame_count % 5 == 0:
            raw = cv2.imencode('.jpg', gray)[1].tostring()
            tx_preview.send(raw)     
        if frame_count % 100 == 0:
            if print_timing:
                print 'detect dt', 1/(time.time() - prev_t)*100
                prev_t = time.time()
            if save:
                cv2.imwrite("raw{0}.jpg".format(frame_count), frame)
        if show_gui:
            gui(frame, associated_faces)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                exit()        
        prev_associated_faces = associated_faces
        processing_flag.value = 1
        frame_count+=1



if __name__ == '__main__':    
    output_p, input_p = Pipe()    
    processing_flag = Value('i', 1) #indicates the consumer is ready to consume
    p = Process(target=detect_worker, args=((output_p, input_p), processing_flag))
    

    def on_image(frame):
        if p.is_alive():      
            if processing_flag.value == 1:   
                input_p.send(frame)                
        else:            
            print 'p is not alive...bailing'
            c.stop()
        
    
    p.start()
    output_p.close() #rx only
    c = camera.camera((320,240), on_image)
    c.run()    
  

    p.join()
    #clean up
    if show_gui:
        cv2.destroyAllWindows()
    
