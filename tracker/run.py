#!/usr/bin/env python
import math, json, time
import cv2
import numpy as np
import util
import cmt
import tsconstants
import tsnetwork

from numpy import empty, nan
from tsconstants import *

gui = True
cmt = cmt.cmt()
cmt.estimate_scale = True
cmt.estimate_rotation = True

def onFaces(msg):
    faces = json.loads(msg)     

rxMgr  = tsnetwork.UdpManager()
rxMgr.register(tsnetwork.UdpReceiver(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT, onFaces))
tx = tsnetwork.UdpSender(LISTEN_IP, FACE_TRACK_IP, FACE_TRACK_PORT)
frame = 1
   
    
def sendResults(tic, toc):
    print '{6:04d}: center: {0:.2f},{1:.2f} scale: {2:.2f}, rot: {3:.2f}, active: {4:03d}, {5:04.0f}ms'.format(cmt.center[0], cmt.center[1], cmt.scale_estimate, cmt.rotation_estimate * 180.0/ math.pi, cmt.active_keypoints.shape[0],  (toc - tic) * 1000.0, frame)
    msg = json.dumps({ 
        "center": {
            "x": cmt.center[0],
            "y": cmt.center[1]
        },
        "scale": cmt.scale_estimate,
        "r": cmt.rotation_estimate,
        "kpcount": cmt.active_keypoints.shape[0]      
    })
    print msg
    tx.send(msg)
    return

if gui:
    cv2.destroyAllWindows()


cap = cv2.VideoCapture(0)
# Check if videocapture is working
if not cap.isOpened():
    print 'Unable to open video input.'
    sys.exit(1)


# Read first frame 
# actually delay since the webcam aparently needs to auto iris a bit
for i in range(100):
    status, im0 = cap.read()
    im_gray0 = cv2.cvtColor(im0, cv2.COLOR_BGR2GRAY)
    im_draw = np.copy(im_gray0)

##TODO: this is the bit we're bootstrapping with the face detector
# Get rectangle input from user
(tl, br) = util.get_rect(im_draw)

print 'using', tl, br, 'as init bb'

#this is what we'll pull in from the face detector
tic = time.time()
cmt.initialise(im_gray0, tl, br)
toc = time.time()

print 'init time is ' , (toc-tic) * 1000 , 'ms'

while True:
    # Read image
    status, im = cap.read()
    if not status:
        break
    im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    im_draw = np.copy(im)

    tic = time.time()
    cmt.process_frame(im_gray)
    toc = time.time()

    # Display results

    # Draw updated estimate
    if gui: 
        if cmt.has_result:
            cv2.line(im_draw, cmt.tl, cmt.tr, (255, 0, 0), 4)
            cv2.line(im_draw, cmt.tr, cmt.br, (255, 0, 0), 4)
            cv2.line(im_draw, cmt.br, cmt.bl, (255, 0, 0), 4)
            cv2.line(im_draw, cmt.bl, cmt.tl, (255, 0, 0), 4)
    
        util.draw_keypoints(cmt.tracked_keypoints, im_draw, (255, 255, 255))

        # this is from simplescale
        util.draw_keypoints(cmt.votes[:, :2], im_draw)  # blue
        util.draw_keypoints(cmt.outliers[:, :2], im_draw, (0, 0, 255))

        if not math.isnan(cmt.center[0]) and not math.isnan(cmt.center[1]):
            cv2.circle(im_draw, 
                    (int( cmt.center[0]), int( cmt.center[1])),
                    3, 
                    (0,255,0))
       
        #cmt.scale_estimate
        #cmt.rotation_estimate 

        cv2.imshow('output', im_draw)
        _ = cv2.waitKey(10)

    # Remember image
    im_prev = im_gray

    # Advance frame number
    frame += 1

    #send stuff over udp
    sendResults(tic,toc)

    #handle udp rx stuffs
    rxMgr.loop()
    