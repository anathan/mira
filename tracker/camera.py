import math, json, time, random, platform
import cv2 
import numpy as np

from numpy import empty, nan

class camera(object):

    def __init__(self, resolution, callback):
        self.resolution = resolution
        self.callback = callback    
        #setup the cameras which is different in each OS
        if platform.system() == "Windows":
            self._camera = cv2.VideoCapture(0)
            self._camera.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,resolution[0])
            self._camera.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,resolution[1])            
        elif platform.system() == "Darwin":
            self._camera = cv2.VideoCapture(0)
            self._camera.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,resolution[0])
            self._camera.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,resolution[1])            
        else:
            import picamera
            from picamera import PiCamera
            from picamera.array import PiRGBArray    
            self._camera = PiCamera(resolution=(1296,972), sensor_mode=4)
            self._camera.vflip = True  
            self._camera.hflip = True
            self._camera.video_stabilization = False#True
            self._camera.meter_mode = 'matrix'
            self._camera.exposure_mode = 'antishake'
            self._camera.drc_strength = 'high'
            #self._camera.contrast = 50
            self._camera.exposure_compensation = 6
            self._raw_capture = PiRGBArray(camera, size=resolution)
        self._running = False
        self.frame_count=0
        self._last_t = time.time()

    def _on_image(self, image):   
        #divisor = 100     
        #if self.frame_count % divisor == 0:
        #    print 'cap', (1/(time.time()-self._last_t))*divisor, 'hz'
        #   self._last_t=time.time()
        self.callback(image)
        self.frame_count+=1

    def _generator(self):        
        while self._running:
            yield self._raw_capture
            frame = (self._raw_capture.array)
            self._on_image(frame)
            self._raw_capture.truncate(0)
    
    def run(self):
        self._running = True
        if platform.system() == "Windows" or platform.system() == "Darwin":
            while self._running:
                status, frame = self._camera.read()  
                self._on_image(frame)
        else:
            self._camera.capture_sequence(self._generator(), use_video_port=True, format="bgr", resize=self.resolution)
    
    def stop(self):
        self._running = False

if __name__ == "__main__":
    def on_new_image(image):
        pass

    c=camera((320,240), on_new_image)
    c.run()
