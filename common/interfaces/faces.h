#ifndef FACES_H_
#define FACES_H_

#include <vector>
#include <string>

#include "opencv2/core/core.hpp"
#include "../rapidjson/rapidjson.h"
#include "../rapidjson/stringbuffer.h"
#include "../rapidjson/document.h" 
#include "../rapidjson/writer.h"
#include "../rapidjson/reader.h"

static std::string toJson(std::vector<cv::Rect>& faces)
{
    //create a face tracking packet
    rapidjson::StringBuffer s;
    rapidjson::Writer<rapidjson::StringBuffer> writer(s);
    writer.StartObject();
    writer.String("faces");
    writer.StartArray();
    for (unsigned int i = 0; i < faces.size(); i++)
    {
        writer.StartObject();
        writer.String("topleft");
        writer.StartObject();
        writer.String("x");
        writer.Int(faces[i].tl().x);
        writer.String("y");
        writer.Int(faces[i].tl().y);
        writer.EndObject();
        writer.String("bottomright");
        writer.StartObject();
        writer.String("x");
        writer.Int(faces[i].br().x);
        writer.String("y");
        writer.Int(faces[i].br().y);
        writer.EndObject();
        writer.EndObject();
    }
    writer.EndArray();
    writer.EndObject();
    return s.GetString();
}
#endif

