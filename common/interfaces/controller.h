#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "../rapidjson/rapidjson.h"
#include "../rapidjson/stringbuffer.h"
#include "../rapidjson/document.h" 
#include "../rapidjson/writer.h"
#include "../rapidjson/reader.h"

//calibration and motor definitions and other lovely constants
const static int YAW_MIN = 1000;
const static int YAW_MAX = 2000;
const static int YAW_RANGE = YAW_MAX - YAW_MIN;
const static int YAW_CENTER = 1600;

const static int PITCH_MIN = 1000;
const static int PITCH_MAX = 1600;
const static int PITCH_RANGE = PITCH_MAX - PITCH_MIN;
const static int PITCH_CENTER = 1450;

const static int ROLL_MIN = 1170;
const static int ROLL_MAX = 2000;
const static int ROLL_RANGE = ROLL_MAX - ROLL_MIN;
const static int ROLL_CENTER = 1600;

#pragma pack(1)
struct CommandPacket {
    uint8_t startByte;
    uint8_t iBody;
    uint8_t rBody;
    uint8_t gBody;
    uint8_t bBody;
    uint8_t ledLeftEye;
    uint8_t ledRightEye;
    uint16_t yawPos;
    uint16_t pitchPos;
    uint16_t rollPos;
    uint8_t soundVol;
    uint16_t soundFreq;
    uint8_t soundMode;
    uint8_t checksumByte;
} ;
#pragma pack()

//this is a "replacement only" method..so only vlaues that are in the json 
//payload will overwrite the passed in command packet.
static bool fromJson(CommandPacket&p, std::string json)
{
    rapidjson::Document d;
    if (d.Parse(json.c_str()).HasParseError())
        return false;
    
    if (d.HasMember("body"))
    {
        p.rBody = d["body"]["r"].GetInt();
        p.gBody = d["body"]["g"].GetInt();
        p.bBody = d["body"]["b"].GetInt();
        p.iBody = d["body"]["i"].GetInt();
    }
    if (d.HasMember("eye"))
    {
        p.ledLeftEye = d["eye"]["left"].GetInt();
        p.ledRightEye = d["eye"]["right"].GetInt();
    }
    if (d.HasMember("head"))
    {
        p.yawPos = d["head"]["yaw"].GetInt();
        p.pitchPos = d["head"]["pitch"].GetInt();
        p.rollPos = d["head"]["roll"].GetInt();
    }
    if (d.HasMember("sound"))
    {
        p.soundMode = d["sound"]["mode"].GetInt();
        p.soundFreq = d["sound"]["freq"].GetInt();
        p.soundVol = d["sound"]["vol"].GetInt();
    }
    return true;
}

static std::string toJson(int yaw, int pitch, int roll)
{
    rapidjson::StringBuffer s;
    rapidjson::Writer<rapidjson::StringBuffer> writer(s);
    writer.StartObject();
        writer.String("head");
        writer.StartObject();
        writer.String("yaw");
        writer.Int(yaw);
        writer.String("pitch");
        writer.Int(pitch);
        writer.String("roll");
        writer.Int(roll);
        writer.EndObject();
    writer.EndObject();
    return s.GetString();
}
static std::string toJson(CommandPacket& p)
{
    rapidjson::StringBuffer s;
    rapidjson::Writer<rapidjson::StringBuffer> writer(s);
    writer.StartObject();
    writer.String("body");
    writer.StartObject();
        writer.String("i");
        writer.Int(p.rBody);
        writer.String("r");
        writer.Int(p.rBody);
        writer.String("g");
        writer.Int(p.gBody);
        writer.String("b");
        writer.Int(p.bBody);
     writer.EndObject();
     writer.String("eye");
     writer.StartObject();
        writer.String("left");
         writer.Int(p.ledLeftEye);
         writer.String("right");
         writer.Int(p.ledRightEye);         
     writer.EndObject();
     writer.String("head");
     writer.StartObject();
         writer.String("yaw");
         writer.Int(p.yawPos);
         writer.String("pitch");
         writer.Int(p.pitchPos);
         writer.String("roll");
         writer.Int(p.rollPos);
     writer.EndObject();
     writer.String("sound");
     writer.StartObject();
         writer.String("mode");
         writer.Int(p.soundMode);
         writer.String("freq");
         writer.Int(p.soundFreq);
         writer.String("vol");
         writer.Int(p.soundVol);      
    writer.EndObject();
    writer.EndObject();
    return s.GetString();
}



#endif
