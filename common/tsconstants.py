# This is a matching template....not a full ip address. If the substring
# is found in the local adapters, that adapter will be used. Note by default
# Mira is configured to issue IP's matching this metric when you connect to her
# internal access point. You can also specify 0.0.0.0 to just bind to "any" although
# this may cause other issues.
import platform
import os.path

MIRA_ID_FILE_NAME = 'mira.txt'

def mira_id():
    node = platform.node()
    #parse the node info apart if it starts with mira
    if node.startswith('Mira') or node.startswith('mira'):
        return int(node[4:])
    elif os.path.isfile(MIRA_ID_FILE_NAME):
        f = open(MIRA_ID_FILE_NAME, 'r')
        idstr = f.readline()
        if idstr.startswith('Mira') or idstr.startswith('mira'):
            return int(idstr[4:])
        else:
            return -1  
    else:
        print 'WARNING: could not determine mira id.'
        return -1

def is_mira():
    return mira_id() != -1


LISTEN_IP = '192.168.42.' 
#LISTEN_IP = '0.0.0.0'

#Multicast addresses for services in Mira 
FACE_DETECT_IP = '224.1.1.1'
FACE_DETECT_PORT = 41149

CONTROLLER_IP = '224.1.1.2'
CONTROLLER_PORT = 41150

BRAIN_STATE_IP = '224.1.1.3'
BRAIN_STATE_PORT = 41151

FACE_TRACK_IP = '224.1.1.4'
FACE_TRACK_PORT = 41152

CAMERA_PREVIEW_IP = '224.1.1.5'
CAMERA_PREVIEW_PORT = 41153

#These are the center points for the detector. Double them to 
#get the full resolution of the face detector
FACE_CENTER_X = 173
FACE_CENTER_Y = 149

#literally degrees per servo count
DEGREE_PER_TICK = .09


class _ConfigContainer(object):

    YAW_MIN = 1000
    YAW_CENTER = 1500
    YAW_MAX = 2000

    PITCH_MIN = 1200
    PITCH_CENTER = 1250
    PITCH_MAX = 1300

    ROLL_MIN = 1400
    ROLL_CENTER = 1500
    ROLL_MAX = 1600

    def __init__(self):
        print '#########CONFIG CONSTRUCTOR#########'
        id = mira_id()     
        #yaw bigger numbers are mira left (cw)
        #pitch bigger numbers point down    
        #roll bigger is tilt mira left down
        if id == 1:
            self.YAW_MIN = 1000
            self.YAW_CENTER = 1580
            self.YAW_MAX = 2000

            self.PITCH_MIN = 1000
            self.PITCH_CENTER = 1400
            self.PITCH_MAX = 1600

            self.ROLL_MIN = 1300
            self.ROLL_CENTER = 1575
            self.ROLL_MAX = 1700
        elif id == 2:
            self.YAW_MIN = 1000
            self.YAW_CENTER = 1625
            self.YAW_MAX = 2000

            self.PITCH_MIN = 1060
            self.PITCH_CENTER = 1610
            self.PITCH_MAX = 1800

            self.ROLL_MIN = 1300
            self.ROLL_CENTER = 1460
            self.ROLL_MAX = 1700
        elif id == 3:
            self.YAW_MIN = 1000
            self.YAW_CENTER = 1540
            self.YAW_MAX = 2000

            self.PITCH_MIN = 1180
            self.PITCH_CENTER = 1540
            self.PITCH_MAX = 1800

            self.ROLL_MIN = 1240
            self.ROLL_CENTER = 1600
            self.ROLL_MAX = 1950
        else:
            print 'WARNING NO CONFIG DEFINED FOR MIRA ', id if id is not None else ' <NO ID DEFINED>'
            
CONFIG = _ConfigContainer()

SOUND_OFF = 0
SOUND_ON = 1
SOUND_VOLUME_DEFAULT = 10


