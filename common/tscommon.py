import json , math, time

"""Common classes and utiltiies used across the stack"""
def scale(pt, amnt):
    (x,y) = pt
    return (x*amnt, y*amnt)

#Creates a point on a coordinate plane with values x and y.
class Point(object):
    
    x = None
    y = None

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self, dx, dy):
        self.x = self.x + dx
        self.y = self.y + dy

    def __str__(self):
        return "Point(%s,%s)"%(self.x, self.y) 

    def distance(self, other):
        dx = self.x - other.x
        dy = self.y - other.y
        return math.sqrt(dx**2 + dy**2)

# "model" for detected face frame
class Face(object):
    
    def __init__(self, tl, br):
        self.tl = tl
        self.br = br
        self.first_detection_time = time.time()
        self.last_miss_time = None
        self.id = 0
        self.tracked = False

    def tl_cv(self):
        return (self.tl.x, self.tl.y)
    def br_cv(self):
        return (self.br.x, self.br.y)

    def getRect(self):
        return (self.tl.x, self.tl.y, self.br.x, self.br.y)

    def getCentroid(self):
        return Point((self.tl.x + self.br.x)/2, (self.br.y + self.tl.y)/2) 

def hsv_to_rgba(h, s, v ):
    curRGB = colorsys.hsv_to_rgb(h,s,v)
    rgb = [0,0,0,0]
    for idx, val in enumerate(curRGB):
        rgb[idx]= int(round(val*255.0))
    rgb.append(0)
    return rgb

#note: for reverse interpolation, you likely want 1.0 - sel
def interp_1d(a,b,sel):
    if a <= b:
       return (b-a) * sel + a
    else:
       return interp_1D(b,a,sel)
       
