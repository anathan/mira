import serial #library to interact with serial port.. very similar to arduino version
import time #for delays

print "Initalizing Serial port..."
arduino = serial.Serial("/dev/ttyAMA0",9600,timeout = 1) # 2=Com3 on windows always a good idea to sepcify a timeout incase we send bad data
time.sleep(2) #wait for initialize
print "Initialization Complete"


class serialPacket:
    startByte = 255
    iBody = 1
    rBody = 255
    gBody = 255
    bBody = 255
    ledLeftEye = 255
    ledRightEye = 255
    yawPos = 1500
    pitchPos = 1500
    rollPos = 1500
    soundVol = 1
    soundFreq = 0
    soundMode = 1

    def send(self):
        soundFreq1=self.soundFreq&255
        soundFreq2=(self.soundFreq&(255<<8))>>8
        yawPos1 = self.yawPos&255
        yawPos2 = (self.yawPos&(255<<8))>>8
        pitchPos1 = self.pitchPos&255
        pitchPos2 = (self.pitchPos&(255<<8))>>8
        rollPos1 = self.rollPos&255
        rollPos2 = (self.rollPos&(255<<8))>>8

        arduino.write(chr(254))
        arduino.write(chr(self.iBody))
        arduino.write(chr(self.rBody))
        arduino.write(chr(self.gBody))
        arduino.write(chr(self.bBody))
        arduino.write(chr(self.ledLeftEye))
        arduino.write(chr(self.ledRightEye))
        arduino.write(chr(yawPos1))
        arduino.write(chr(yawPos2))
        arduino.write(chr(pitchPos1))
        arduino.write(chr(pitchPos2))
        arduino.write(chr(rollPos1))
        arduino.write(chr(rollPos2))
        arduino.write(chr(self.soundVol))
        arduino.write(chr(soundFreq1))
        arduino.write(chr(soundFreq2))
        arduino.write(chr(self.soundMode))
        byteSum = 252
        #byteSum = 254+self.iBody+self.rBody+self.gBody+self.bBody+self.ledLeftEye+self.ledRightEye+yawPos1+yawPos2+pitchPos1+pitchPos2+rollPos1+rollPos2+self.soundVol+self.soundFreq+self.soundMode
        self.checksumByte = byteSum&255
        arduino.write(chr(self.checksumByte))
