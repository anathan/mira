import numpy as np
import time
import math

def millis():
    return int(round(time.clock()*1000))

class Word:
    def __init__(self,*args):
        argsSize = len(args)
        pitch = args[0]
        if  argsSize == 3:
            self.start_pitch = args[0]
            self.end_pitch = args[0]
            self.volume = args[1]
            self.duration = args[2]
        if  argsSize == 4:
            self.start_pitch = args[0]
            self.end_pitch = args[1]
            self.volume = args[2]
            self.duration = args[3]

class Phrase:
    def __init__(self,wordArray):
        self.wordArray = wordArray
        self.wordArray.append(Word(0,0,200))

    def offsetPitch(self, offset):
        for i in self.wordArray:
            if i.start_pitch != 0:
                i.start_pitch += offset
                i.end_pitch += offset

    def offsetDuration(self, offset):
        for i in self.wordArray:
            i.duration *= offset

    def offsetVolume(self, offset):
        for i in self.wordArray:
            i.volume *= np.clip(offset,0,10)

    def setVolume(self, value):
        for i in self.wordArray:
            i.volume = value    


def HSVtoRGB(h, s, v ):
    r = 0.0
    g = 0.0
    b = 0.0
    i = 0
    f = 0.0
    p = 0.0
    q = 0.0
    t = 0.0
    if s == 0  :
        # achromatic (grey)
        r = g = b = v
        return
    h *= 5         # sector 0 to 5
    i = math.floor( h )
    f = h - float(i)      # factorial part of h
    p = v * ( 1 - s )
    q = v * ( 1 - s * f )
    t = v * ( 1 - s * ( 1 - f ) )
    if i == 0:
        r = v
        g = t
        b = p
    elif i == 1:
        r = q
        g = v
        b = p
    elif i == 2:
        r = p
        g = v
        b = t
    elif i == 3:
        r = p
        g = q
        b = v
    elif i == 4:
        r = t
        g = p
        b = v
    else:        
        r = v
        g = p
        b = q
    r = np.clip(int(r*255.0),0,255)
    g = np.clip(int(g*255.0),0,255)
    b = np.clip(int(b*255.0),0,255)

    return [r,g,b]


phrase1 = [Word(0,10,387), Word(499,10,163), Word(0,10,417), Word(475,10,192), Word(0,10,387), Word(504,10,29), Word(504,10,87), Word(505,10,19), Word(0,10,357), Word(528,10,18), Word(0,10,272), Word(502,10,19), Word(0,10,126), Word(488,10,29), Word(488,10,48), Word(0,10,272), Word(468,10,57), Word(470,10,18), Word(0,10,156), Word(509,10,29), Word(509,10,47), Word(0,10,270), Word(540,10,28), Word(540,10,18), Word(0,10,185), Word(586,10,29), Word(588,10,29), Word(588,10,29), Word(587,10,18), Word(0,10,243), Word(555,10,29), Word(555,10,47), Word(0,10,128), Word(575,10,29), Word(575,10,87), Word(573,10,18), Word(0,10,212), Word(545,10,30), Word(545,10,47), Word(0,10,272), Word(489,10,29), Word(489,10,18), Word(0,10,127), Word(508,10,28), Word(509,10,29), Word(508,10,18), Word(0,10,128), Word(492,10,29), Word(490,10,29), Word(489,10,29), Word(489,10,29), Word(490,10,18), Word(0,10,896), Word(0,10,127), Word(535,10,29), Word(534,10,18), Word(0,10,127), Word(519,10,29), Word(519,10,18), Word(0,10,155), Word(492,10,58), Word(491,10,18), Word(0,10,242), Word(533,10,29), Word(533,10,47), Word(0,10,358), Word(488,10,47), Word(0,10,155), Word(508,10,29), Word(508,10,29), Word(510,10,18), Word(0,10,98), Word(517,10,29), Word(518,10,29), Word(520,10,18), Word(0,10,296), Word(485,10,29), Word(487,10,18), Word(0,10,186), Word(508,10,18), Word(0,10,98), Word(524,10,29), Word(525,10,17), Word(0,10,583), Word(559,10,29), Word(559,10,18), Word(0,10,126), Word(531,10,29), Word(529,10,29), Word(527,10,18), Word(0,10,154), Word(504,10,30), Word(504,10,18), Word(0,10,127), Word(487,10,29), Word(487,10,46), Word(0,10,326), Word(527,10,29), Word(527,10,74), Word(0,10,268), Word(615,9,28), Word(615,9,28), Word(614,9,18), Word(0,9,345), Word(0,9,182), Word(607,9,47)] 

phrase2 = [Word(394,10,30), Word(393,10,18), Word(0,10,214), Word(349,10,29), Word(348,10,28), Word(348,10,175), Word(349,10,28), Word(349,10,204), Word(350,10,18), Word(0,10,271), Word(401,10,29), Word(402,10,18), Word(0,10,126), Word(416,10,30), Word(416,10,18), Word(0,10,127), Word(433,10,29), Word(433,10,58), Word(435,10,18), Word(0,10,98), Word(442,10,29), Word(443,10,29), Word(444,10,18), Word(0,10,98), Word(461,10,29), Word(461,10,580), Word(459,10,18), Word(0,10,156), Word(460,10,28), Word(460,10,29), Word(461,10,18), Word(0,10,174), Word(0,10,126), Word(445,10,48), Word(0,10,187), Word(414,10,29), Word(416,10,18), Word(0,10,157), Word(437,10,29), Word(438,10,29), Word(438,10,18), Word(0,10,274), Word(401,10,28), Word(401,10,19), Word(0,10,155), Word(383,10,30), Word(383,10,57), Word(385,10,30), Word(386,10,28), Word(386,10,203), Word(387,10,30), Word(387,10,308), Word(0,10,214), Word(392,10,77), Word(0,10,97), Word(393,10,30), Word(390,10,29), Word(390,10,18), Word(0,10,126), Word(376,10,29), Word(376,10,18), Word(0,10,127), Word(366,10,29), Word(365,10,19), Word(0,10,126), Word(355,10,29), Word(357,10,29), Word(360,10,19), Word(0,10,126), Word(378,10,30), Word(381,10,18), Word(0,10,126), Word(396,10,30), Word(399,10,17), Word(0,10,696), Word(0,10,214), Word(416,10,29), Word(416,10,18), Word(0,10,128), Word(420,10,46), Word(0,10,127), Word(396,10,29), Word(396,10,29), Word(397,10,18), Word(0,10,127), Word(383,10,30), Word(383,10,28), Word(379,10,18), Word(0,10,98), Word(351,10,30), Word(350,10,29), Word(352,10,17), Word(0,10,127), Word(374,10,29), Word(379,10,18), Word(0,10,127), Word(395,10,29), Word(396,10,18), Word(0,10,562), Word(404,10,29), Word(404,10,76), Word(0,10,214), Word(402,10,29), Word(402,10,29), Word(403,10,18), Word(0,10,156), Word(403,10,18), Word(0,10,127), Word(385,10,29), Word(385,10,18), Word(0,10,156), Word(364,10,29), Word(364,10,18), Word(0,10,127), Word(342,10,30), Word(343,10,29), Word(346,10,29), Word(351,10,18), Word(0,10,98), Word(366,10,29), Word(368,10,18), Word(0,10,174), Word(0,10,159), Word(383,10,376), Word(382,10,29), Word(382,10,319), Word(383,10,18), Word(0,10,359), Word(415,10,29), Word(414,10,29), Word(414,10,145), Word(415,10,29), Word(415,10,116), Word(416,10,18), Word(0,10,290), Word(0,10,214), Word(334,10,29), Word(336,10,18), Word(0,10,127), Word(336,10,29), Word(336,10,47), Word(0,10,97), Word(335,10,29), Word(335,10,18), Word(0,10,98), Word(339,10,29), Word(339,10,18), Word(0,10,358), Word(334,10,18), Word(0,10,98), Word(335,10,29), Word(335,10,77), Word(0,10,69), Word(335,10,76), Word(0,10,272), Word(334,10,29), Word(335,10,18), Word(0,10,173), Word(0,10,69), Word(335,10,29), Word(334,10,29), Word(334,10,29), Word(335,10,18), Word(0,10,214), Word(333,10,29), Word(332,10,29), Word(333,10,18), Word(0,10,243), Word(286,10,29), Word(286,10,29), Word(287,10,29), Word(287,10,116), Word(288,10,18), Word(0,10,359), Word(403,10,29)]

phrase3 = [Word(298,8,29), Word(299,8,29), Word(299,8,47), Word(0,8,69), Word(298,8,29), Word(297,8,29), Word(296,8,29), Word(296,8,18), Word(0,8,127), Word(260,8,29), Word(262,8,18), Word(0,8,98), Word(265,8,29), Word(265,8,29), Word(267,8,18), Word(0,8,127), Word(289,8,29), Word(288,8,29), Word(288,8,18), Word(0,8,69), Word(289,8,29), Word(287,8,30), Word(287,8,47), Word(0,8,1810), Word(299,8,76), Word(0,8,127), Word(297,8,29), Word(297,8,18), Word(0,8,156), Word(263,8,29), Word(264,8,29), Word(265,8,18), Word(0,8,69), Word(263,8,29), Word(263,8,47), Word(0,8,127), Word(283,8,29), Word(283,8,76), Word(0,8,69), Word(283,8,29), Word(280,8,29), Word(280,8,29), Word(282,8,18), Word(0,8,1054), Word(300,9,18), Word(0,9,174), Word(0,9,98), Word(273,9,29), Word(271,9,29), Word(270,9,29), Word(271,9,18), Word(0,9,272), Word(305,9,28), Word(304,9,30), Word(305,9,18), Word(0,9,127), Word(282,9,29), Word(280,9,18), Word(0,9,127), Word(286,9,29), Word(287,9,29), Word(287,9,18), Word(0,9,185), Word(348,9,47), Word(0,9,127), Word(332,9,29), Word(327,9,29), Word(324,9,18), Word(0,9,127), Word(313,9,29), Word(313,9,47), Word(0,9,185), Word(372,9,29), Word(372,9,18), Word(0,9,156), Word(351,9,29), Word(347,9,29), Word(345,9,18), Word(0,9,127), Word(339,9,29), Word(340,9,29), Word(345,9,18), Word(0,9,156), Word(410,9,29), Word(408,9,29), Word(407,9,18), Word(0,9,99), Word(400,9,29), Word(397,9,29), Word(395,9,18), Word(0,9,126), Word(362,9,30), Word(362,9,47), Word(0,9,388), Word(72,9,29), Word(71,9,29), Word(70,9,18), Word(0,9,127), Word(54,9,29), Word(51,9,29), Word(49,9,18), Word(0,9,174), Word(0,9,127), Word(16,9,29), Word(15,9,29), Word(15,9,77)]

phraseArray = [Phrase(phrase1), Phrase(phrase2),Phrase(phrase3)]