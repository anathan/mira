from python2ArduinoSerial import *
from miraSpeech import *
import time
import numpy as np

YAW_MIN = 1000
YAW_MAX = 1800
YAW_RANGE = YAW_MAX - YAW_MIN
YAW_CENTER = YAW_RANGE / 2 + YAW_MIN

PITCH_MIN = 1075
PITCH_MAX = 1600
PITCH_RANGE = PITCH_MAX - PITCH_MIN
PITCH_CENTER = PITCH_RANGE / 2 + PITCH_MIN

ROLL_MIN = 1200
ROLL_MAX = 1800
ROLL_RANGE = ROLL_MAX - ROLL_MIN
ROLL_CENTER = ROLL_RANGE / 2 + ROLL_MIN

mira =  serialPacket()


def playWord(thisWord):
    beginTime = millis()
    endTime = beginTime + thisWord.duration
    curTime = beginTime
    chunkTime = 20  
    while (curTime < endTime) :
        timeToPlayChunk = min(chunkTime, endTime - curTime)
        WordPercent = (curTime - beginTime) / float(endTime - beginTime)
        curPitch = WordPercent * thisWord.end_pitch + (1 - WordPercent) * thisWord.start_pitch
        mira.soundFreq = int(curPitch)
        mira.soundVol = int(thisWord.volume)
        rgb = HSVtoRGB(np.clip(mira.soundFreq/1000.0,0.0,1.0),1.0,1.0)
        mira.rBody = rgb[0]
        mira.gBody = rgb[1]
        mira.bBody = rgb[2]
        feedback = arduino.readline()
        print feedback
        time.sleep(timeToPlayChunk/1000)
        curTime = millis()
        if mira.soundFreq == 0:
            mira.soundMode = 0
        else:
            mira.soundMode = 1


'''
phraseArray[1].offsetPitch(100)
phraseArray[1].offsetDuration(.6)
phraseArray[1].setVolume(1)
phraseArray[0].offsetPitch(100)
phraseArray[0].offsetDuration(.6)
'''

while 1:
    for i in range(len(phraseArray)):
        for j in range(len(phraseArray[i].wordArray)):
            playWord(phraseArray[i].wordArray[j])
            j += 1
            mira.send()
        print "Next Song"
        time.sleep(2)

        i += 1
    print 'End of songs'
    arduino.close()
    print 'byebye'



