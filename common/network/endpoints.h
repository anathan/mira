#ifndef ENDPOINTS_H_
#define ENDPOINTS_H_

//These are the multicast listen IP's for receivers and transmitters. They should match the IP of the adapter you want to listen on, 
//where 0.0.0.0 is the special case that uses "whatever".
#define LISTEN_IP_AUTO        "0.0.0.0"
#define LISTEN_IP_MIRA        "192.168.42.1"
#define LISTEN_IP_LOCAL       "127.0.0.1"

#define FACE_DETECT_PORT  41149
#define FACE_DETECT_IP   "224.1.1.1"

#define CONTROLLER_PORT  41150
#define CONTROLLER_IP    "224.1.1.2"

#endif