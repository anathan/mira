#ifndef _RECEIVER_H_
#define _RECEIVER_H_

#include <iostream>
#include <string>
#include <list>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/foreach.hpp>

class Receiver
{
public:
    typedef boost::function<void(std::string)> msgCallbackType;
    Receiver(boost::asio::io_service& io_service, const boost::asio::ip::address& listen_address, const boost::asio::ip::address& multicast_address, const unsigned short multicast_port);
    void handle_receive_from(const boost::system::error_code& error, size_t bytes_recvd);
    void register_callback(msgCallbackType cb);
private:
    
    boost::asio::ip::udp::socket socket_;
    boost::asio::ip::udp::endpoint sender_endpoint_;
    enum { max_length = 65535 };
    char data_[max_length];
    std::list<msgCallbackType> cbs;
};

#endif