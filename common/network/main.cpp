#include <iostream>
#include <string>
#include <sstream>

//note: asio should be included first due to jenkiness in windows.h
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "../rapidjson/rapidjson.h"
#include "../rapidjson/stringbuffer.h"
#include "../rapidjson/writer.h"
#include "../rapidjson/prettywriter.h" // for stringify JSON
#include "../interfaces/controller.h"
#include "sender.h"
#include "receiver.h"

int port = 41150;
const char* ip = "224.1.1.2";
const char* listen_ip = "0.0.0.0";

void onReceive(std::string s)
{
    std::cout << "RX: " << s << std::endl;
}

int main(int argc, char* argv[])
{
    try
    {

        boost::asio::io_service io_service;
        
        receiver r(io_service,
            boost::asio::ip::address::from_string(listen_ip),
            boost::asio::ip::address::from_string(ip),
            port);
            
        r.register_callback(boost::bind(&onReceive, _1));

        sender sender(io_service, boost::asio::ip::address::from_string(ip), port);

        boost::thread bt(boost::bind(&boost::asio::io_service::run, &io_service));
        
        while (true)
        {
            CommandPacket p; 
            p.rBody = p.gBody = p.bBody = 225;
            p.soundFreq = 300;
            p.soundMode = 1;
            p.soundVol = 10;
            std::string json = toJson(p);
            CommandPacket p2;
            fromJson(p2, json);
            sender.send(json);
            
            boost::this_thread::sleep(boost::posix_time::milliseconds(1000));
            std::cout << "TX: " << toJson(p2) << std::endl;
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
        std::string test;
        std::cin >> test;
    }

    return 0;
}

