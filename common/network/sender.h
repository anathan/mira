
#include <iostream>
#include <sstream>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include "boost/date_time/posix_time/posix_time_types.hpp"

class Sender
{
public:
    Sender(boost::asio::io_service& io_service, std::string interface_address, std::string multicast_address, const int multicast_port);
    void send(std::string const& msg);
    void handle_send_to(boost::shared_ptr<std::string> s, boost::system::error_code const&, size_t);


private:
    boost::asio::ip::udp::endpoint endpoint_;
    boost::asio::ip::udp::socket socket_;
    std::string message_;    
    
};
