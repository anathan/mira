import tsnetwork, time

def onFace(msg):
    print "F: " + msg

def onCtrl(msg):
    print "C: " + msg

LISTEN_IP = "0.0.0.0"
FACE_DETECT_IP = '224.1.1.1'
FACE_DETECT_PORT = 41149

CONTROLLER_IP = '224.1.1.2'
CONTROLLER_PORT = 41150

man  = tsnetwork.UdpManager()
tx = tsnetwork.UdpSender(LISTEN_IP, CONTROLLER_IP, CONTROLLER_PORT)
tx.send('{}')

man.register(tsnetwork.UdpReceiver(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT, onFace))
man.register(tsnetwork.UdpReceiver(LISTEN_IP, CONTROLLER_IP, CONTROLLER_PORT, onCtrl))


while True:

    man.loop()
    time.sleep(0.01)
#    print "heatbeat"
    
