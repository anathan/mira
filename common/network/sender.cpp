#include "sender.h"

using namespace boost::asio;

Sender::Sender(io_service& io_service, std::string interface_address, std::string multicast_address, const int multicast_port) :
    endpoint_(ip::address::from_string(multicast_address), multicast_port),
    socket_(io_service, endpoint_.protocol())
{
 
    // SO_BINDTODEVICE				
    /*if ((::setsockopt(socket_.native(), SOL_SOCKET, SO_BINDTODEVICE, ifp_->name().c_str(), ifp_->name().size() + 1) == -1))
    {
        throw std::runtime_error("setsockopt() SO_BINDTODEVICE");
    }*/

    // bind
    socket_.set_option(ip::udp::socket::reuse_address(true));
    //socket_.bind(endpoint_);

    // disable loopback
    //socket_.set_option(ip::multicast::enable_loopback(false));

    // set oif - the socket will use this interface as outgoing interface
    ip::address if_address = ip::address::from_string(interface_address);
    socket_.set_option(ip::multicast::outbound_interface(if_address.to_v4()));

    // set mcast group - join group - 				
    socket_.set_option(ip::multicast::join_group(ip::address::from_string(multicast_address).to_v4(), if_address.to_v4()));  
    // open the socket
    //socket_.open(endpoint_.protocol());
}

void Sender::send(std::string const& msg)
{ 
    boost::shared_ptr<std::string> s = boost::make_shared<std::string>(msg);
    socket_.async_send_to(
        boost::asio::buffer(*s), endpoint_,
        boost::bind(&Sender::handle_send_to, this, s, _1, _2));
}

void Sender::handle_send_to(boost::shared_ptr<std::string> s, boost::system::error_code const& e, size_t sz)
{
    if (e)
        std::cout << "Error sending message! " << e << std::endl;
}

