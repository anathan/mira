import socket, struct, platform, select

from netifaces import interfaces, ifaddresses, AF_INET

def ip4_addresses():
    ip_list = []
    for interface in interfaces():
        if AF_INET in ifaddresses(interface):
            for link in ifaddresses(interface)[AF_INET]:
                ip_list.append(link['addr'])
    return ip_list

class UdpManager(object):
    def __init__(self):
        print "[UDP Manager initiailized]"
        #key the receiver by their sock object
        #since thats what select gives us
        self.receivers = dict()
        self.bufferSize = 65535

    def register(self, receiver):
        self.receivers[receiver.sock] = receiver
        
    def loop(self):
        #get the socket for each udp reciever and poll on it
        result = select.select(iter(self.receivers),[],[],0)     
        for sock in result[0]:            
            udpRx = self.receivers[sock]
            msg = sock.recv(self.bufferSize) 
            udpRx.callback(msg)

        #TODO: we must handle the case where we get dead sockets or this will get very unhappy


class UdpSender(object):
   
    def __init__(self, interface_ip, multicast_ip, multicast_port):
        if interface_ip == '0.0.0.0':
            print '[UDP WARN: you are binding to interface ANY (0.0.0.0)!'
        else:
            ipsMatch = [ip for ip in ip4_addresses() if interface_ip in ip]
            ip = ipsMatch[0] if ipsMatch else None
            interface_ip = ip
        if (interface_ip == None):
            raise SystemError('Could not find an adapter to bind to matching the specified local ip. Make sure you are connected to mira.')
         
        self.multicast_ip = multicast_ip
        self.multicast_port = multicast_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
        self.sock.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(interface_ip))                       
        print "[UDP Sender @ " + str(multicast_ip) + ":" + str(multicast_port) + " on interface " + interface_ip + "]"
   
    #sends a message over the wire 
    def send (self, message):
        self.sock.sendto("" + message + "", (self.multicast_ip, self.multicast_port))



class UdpReceiver(object):
    def __init__(self, interface_ip, multicast_ip, multicast_port, msghandler):
        try:
            if interface_ip == '0.0.0.0':
                print '[UDP WARN: you are binding to interface ANY (0.0.0.0)!'
            else:
                ipsAll = ip4_addresses();
                ipsMatch = [ip for ip in ipsAll if interface_ip in ip]
                ip = ipsMatch[0] if ipsMatch else None
                interface_ip = ip

            self.msghandler = msghandler
            self.__sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            try:
                self.__sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            except AttributeError:
                pass

            #note we may need SO_REUSEPORT  on OSX
            self.__sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 32) 
            self.__sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 1)
            if (interface_ip == None):
                raise SystemError('Could not find an adapter to bind to matching the specified local ip. Make sure you are connected to mira.')
            #bind only to the local
            self.__sock.setsockopt(socket.SOL_IP, socket.IP_MULTICAST_IF, socket.inet_aton(interface_ip))

            if platform.system() == "Windows":
                self.__sock.bind((interface_ip, multicast_port))
            else:
                self.__sock.bind((multicast_ip, multicast_port)) #linux and mac seem to want to bind the mcast address, bizzare.
                #send a bind request  
            self.__sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_ip) + socket.inet_aton(interface_ip))
            #we want the socket to be non-blocking on receive so we can select() on it
            self.__sock.setblocking(0)
            print "[UDP Server Running @ " + str(multicast_ip) + ":" + str(multicast_port) +  " on interface " + interface_ip + "]"
        except socket.error as detail:
            print "Doh, UDP Port binding fail.", detail
            raise
            exit(1)

    @property
    def sock(self):
        return self.__sock

    def callback(self, msg):
        self.msghandler(msg)

    #def sock(self):
    #    return self.sock

    #def msghandler(self):
    #    return self.msghandler


