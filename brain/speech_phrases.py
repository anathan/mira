import copy

from speech_notes import *

class Word:
    def __init__(self,*args):
        argsSize = len(args)
        pitch = args[0]
        if  argsSize == 3:
            self.start_pitch = args[0]
            self.end_pitch = args[0]
            self.volume = args[1]
            self.duration = args[2]
        if  argsSize == 4:
            self.start_pitch = args[0]
            self.end_pitch = args[1]
            self.volume = args[2]
            self.duration = args[3]

class Phrase:
    def __init__(self,logarithmic,wordArray):
        self.wordArray = wordArray
        self.wordArray.append(Word(0,0,200))
        self.logarithmic = logarithmic

    def offsetPitch(self, offset):
        w = copy.deepcopy(self.wordArray)
        for i in w:
            if i.start_pitch != 0:
                i.start_pitch += offset
                i.end_pitch += offset
        return Phrase(self.logarithmic, w)

    def offsetDuration(self, offset):
        w = copy.deepcopy(self.wordArray)
        for i in w:
            i.duration *= offset
        return Phrase(self.logarithmic, w)

    def offsetVolume(self, offset):
        w = copy.deepcopy(self.wordArray)
        for i in w:
            i.volume *= np.clip(offset,0,10)
        return Phrase(self.logarithmic, w)

    def setVolume(self, value):
        w = copy.deepcopy(self.wordArray)
        for i in w:
            i.volume = value  
        return Phrase(self.logarithmic, w)

mario = Phrase(True, [Word(0,10,387), Word(499,10,163), Word(0,10,417), Word(475,10,192), Word(0,10,387), Word(504,10,29), Word(504,10,87), Word(505,10,19), Word(0,10,357), Word(528,10,18), Word(0,10,272), Word(502,10,19), Word(0,10,126), Word(488,10,29), Word(488,10,48), Word(0,10,272), Word(468,10,57), Word(470,10,18), Word(0,10,156), Word(509,10,29), Word(509,10,47), Word(0,10,270), Word(540,10,28), Word(540,10,18), Word(0,10,185), Word(586,10,29), Word(588,10,29), Word(588,10,29), Word(587,10,18), Word(0,10,243), Word(555,10,29), Word(555,10,47), Word(0,10,128), Word(575,10,29), Word(575,10,87), Word(573,10,18), Word(0,10,212), Word(545,10,30), Word(545,10,47), Word(0,10,272), Word(489,10,29), Word(489,10,18), Word(0,10,127), Word(508,10,28), Word(509,10,29), Word(508,10,18), Word(0,10,128), Word(492,10,29), Word(490,10,29), Word(489,10,29), Word(489,10,29), Word(490,10,18), Word(0,10,896), Word(0,10,127), Word(535,10,29), Word(534,10,18), Word(0,10,127), Word(519,10,29), Word(519,10,18), Word(0,10,155), Word(492,10,58), Word(491,10,18), Word(0,10,242), Word(533,10,29), Word(533,10,47), Word(0,10,358), Word(488,10,47), Word(0,10,155), Word(508,10,29), Word(508,10,29), Word(510,10,18), Word(0,10,98), Word(517,10,29), Word(518,10,29), Word(520,10,18), Word(0,10,296), Word(485,10,29), Word(487,10,18), Word(0,10,186), Word(508,10,18), Word(0,10,98), Word(524,10,29), Word(525,10,17), Word(0,10,583), Word(559,10,29), Word(559,10,18), Word(0,10,126), Word(531,10,29), Word(529,10,29), Word(527,10,18), Word(0,10,154), Word(504,10,30), Word(504,10,18), Word(0,10,127), Word(487,10,29), Word(487,10,46), Word(0,10,326), Word(527,10,29), Word(527,10,74), Word(0,10,268), Word(615,9,28), Word(615,9,28), Word(614,9,18), Word(0,9,345), Word(0,9,182), Word(607,9,47)]) 
 
cmajorbounce = Phrase (False,  [Word(NOTE_C4,10,100),  Word(NOTE_C5,10,100)])

cmajor = Phrase(False, [Word(NOTE_C4,10,100), 
                Word(NOTE_D4,10,100), 
                Word(NOTE_E4,10,100), 
                Word(NOTE_F4,10,100), 
                Word(NOTE_G4,10,100), 
                Word(NOTE_A4,10,100), 
                Word(NOTE_B4,10,100), 
                Word(NOTE_C5,10,100)])

cmajorTriad = Phrase(False, [
                Word(NOTE_C4,10,100), 
                Word(NOTE_E4,10,100), 
                Word(NOTE_G4,10,100), 
                Word(NOTE_C5,10,100)])

cmajorOctave = Phrase(False, [Word(NOTE_C4,10,100), 
                Word(NOTE_C5,10,100)])

aminor = Phrase(False, 
                [
                Word(NOTE_A4,10,100), 
                Word(NOTE_G4,10,100), 
                Word(NOTE_F4,10,100), 
                Word(NOTE_E4,10,100), 
                Word(NOTE_D4,10,100),
                Word(NOTE_C4,10,100),
                Word(NOTE_B3,10,100),
                Word(NOTE_A3,10,100)])

aminorTriad = Phrase(False, 
                [
                Word(NOTE_A4,10,100), 
                Word(NOTE_E4,10,100), 
                Word(NOTE_C4,10,100),
                Word(NOTE_A3,10,100)])

question = Phrase(False,
                  [
                  Word(1661,8,50), 
                  Word(0,8,149), 
                  Word(1480,8,102), 
                  Word(0,8,149)])


laughSoft= Phrase(False, 
                [
Word(757,6,16), Word(768,6,18), Word(784,6,16), Word(792,6,17), Word(818,6,17), Word(826,6,16), Word(829,6,1), Word(0,6,166), Word(778,6,16), Word(789,6,17), Word(805,6,16), Word(821,6,16), Word(832,6,18), Word(840,6,0), Word(0,6,150), Word(781,6,17), Word(808,6,16), Word(826,6,17), Word(842,6,16), Word(858,6,1), Word(0,8,150)])

laughMedium = Phrase(False, 
                [
 Word(512,6,15), Word(544,6,18), Word(626,6,18), Word(664,6,17), Word(797,6,12), Word(845,6,18), Word(986,6,17), Word(1104,7,20), Word(1162,7,2), Word(0,7,161), Word(778,7,16), Word(805,7,16), Word(890,7,17), Word(928,7,19), Word(1048,7,17), Word(1149,7,18), Word(1189,7,1), Word(0,7,162), Word(704,7,17), Word(752,7,16), Word(818,7,17), Word(912,7,24), Word(1064,7,9), Word(1194,7,17), Word(1226,7,17), Word(1269,7,16), Word(1280,7,1), Word(0,7,152), Word(856,7,15), Word(938,7,15), Word(1021,7,19), Word(1117,7,18), Word(1208,7,15), Word(1314,7,16), Word(1338,7,17), Word(1373,7,1), Word(0,7,183), Word(957,7,15), Word(1021,7,17), Word(1141,7,17), Word(1184,7,16), Word(1285,7,17), Word(1328,7,17), Word(1344,7,1), Word(0,7,582), Word(600,6,17), Word(605,6,16), Word(648,6,17), Word(706,6,16), Word(784,6,17), Word(874,6,17), Word(965,6,18), Word(1088,6,15), Word(1141,6,0), Word(0,6,166), Word(709,6,18), Word(744,6,16), Word(800,6,17), Word(917,6,16), Word(997,6,17), Word(1066,6,17), Word(1112,6,1), Word(0,6,149), Word(762,6,16), Word(829,6,17), Word(896,6,17), Word(968,6,15), Word(1032,6,18), Word(1032,6,15), Word(1104,6,1), Word(0,6,201), Word(704,6,16), Word(800,6,17), Word(872,6,17), Word(936,6,17), Word(981,6,16), Word(1005,6,1), Word(0,8,150)])

laughHard= Phrase(False, 
                [
Word(858,6,15), Word(880,6,18), Word(888,6,16), Word(925,6,18), Word(957,6,17), Word(1013,6,15), Word(1032,6,16), Word(1066,6,17), Word(1098,6,17), Word(1133,6,17), Word(1184,6,17), Word(1229,6,16), Word(1258,6,18), Word(1282,6,15), Word(1320,6,17), Word(1346,6,17), Word(1373,6,17), Word(1397,6,17), Word(1421,6,15), Word(1453,6,17), Word(1474,6,17), Word(1496,6,16), Word(1514,6,17), Word(1533,6,17), Word(1554,6,17), Word(1565,6,17), Word(1573,6,17), Word(1578,6,16), Word(1581,6,16), Word(1584,6,17), Word(1584,6,17), Word(1586,6,16), Word(1586,6,18), Word(1589,6,16), Word(1592,6,16), Word(1592,6,17), Word(1594,6,18), Word(1594,6,17), Word(1584,6,0), Word(0,6,183), Word(1306,6,17), Word(1328,6,15), Word(1373,6,16), Word(1405,6,17), Word(1432,6,17), Word(1450,6,17), Word(1461,6,1), Word(0,6,167), Word(1317,6,15), Word(1338,6,18), Word(1360,6,15), Word(1386,6,18), Word(1408,6,16), Word(1429,6,16), Word(1440,6,17), Word(1442,6,1), Word(0,6,149), Word(1232,6,17), Word(1253,6,17), Word(1306,6,16), Word(1357,6,17), Word(1402,6,17), Word(1442,6,16), Word(1464,6,17), Word(1469,6,1), Word(0,6,149), Word(1178,6,16), Word(1186,6,17), Word(1210,6,17), Word(1245,6,16), Word(1288,6,17), Word(1344,6,17), Word(1370,6,16), Word(1384,6,1), Word(0,6,183), Word(1152,6,16), Word(1205,6,17), Word(1234,6,18), Word(1261,6,15), Word(1282,6,18), Word(1296,6,1), Word(0,6,165), Word(1136,6,16), Word(1152,6,17), Word(1160,6,16), Word(1189,6,17), Word(1210,6,17), Word(1229,6,2), Word(0,6,181), Word(1072,6,17), Word(1093,6,17), Word(1109,6,16), Word(1128,6,17), Word(1144,6,17), Word(1160,6,1), Word(0,6,183), Word(1066,6,16), Word(1077,6,17), Word(1093,6,16), Word(1114,6,17), Word(1133,6,16), Word(1144,6,18), Word(1154,6,0), Word(0,6,183), Word(1058,6,16), Word(1069,6,17), Word(1080,6,18), Word(1093,6,17), Word(1096,6,1), Word(0,6,214), Word(1016,6,19), Word(1016,6,31), Word(1013,6,17), Word(1013,6,34), Word(1018,6,1),Word(0,8,150)])
