import json, math, random, copy

from tscommon import Point, Face
from tsconstants import *
from speech_phrases import *
from transitions import Machine

import numpy

#FSM: state label constants
LOOKING_STATE = 'looking'
FOLLOWING_STATE = 'following'
ENGAGING_STATE = 'engaging'
WAITING_STATE = 'waiting'
CHEERING_STATE = 'cheering'


class PeekABooBehavior(object):
    """Performs simple face tracking using input from the c++ facedetect"""

    #FSM: define states 
    states = [
        LOOKING_STATE, 
        FOLLOWING_STATE, 
        ENGAGING_STATE, 
        WAITING_STATE, 
        CHEERING_STATE
    ]

    #FSM: define triggers to transition between states, including conditional checks
    transitions = [
        { 'trigger': 'transition_state', 'source': LOOKING_STATE, 'dest': FOLLOWING_STATE, 'conditions': 'is_tracking_face' },
        { 'trigger': 'transition_state', 'source': FOLLOWING_STATE, 'dest': LOOKING_STATE, 'unless': 'is_tracking_face'},
        { 'trigger': 'transition_state', 'source': FOLLOWING_STATE, 'dest': ENGAGING_STATE, 'conditions': 'is_tracked_face_still' },
        { 'trigger': 'transition_state', 'source': ENGAGING_STATE, 'dest': FOLLOWING_STATE, 'conditions': 'is_tracked_face_unstill' },        
        { 'trigger': 'transition_state', 'source': ENGAGING_STATE, 'dest': WAITING_STATE, 'conditions': 'did_face_disappear', 'after': 'start_waiting' },
        { 'trigger': 'transition_state', 'source': WAITING_STATE, 'dest': CHEERING_STATE, 'conditions': 'did_face_reappear', 'after': 'stop_waiting' },
        { 'trigger': 'transition_state', 'source': WAITING_STATE, 'dest': LOOKING_STATE, 'conditions': 'waiting_too_long', 'before': 'complain', 'after': 'stop_waiting' },
        { 'trigger': 'transition_state', 'source': CHEERING_STATE, 'dest': LOOKING_STATE, 'conditions': 'is_done_cheering' }
    ]

    deadZone = 2
    yawEffort = -0.8 #-.2
    pitchEffort = -0.5
    yawValue = CONFIG.YAW_CENTER
    pitchValue = CONFIG.PITCH_CENTER
    rollValue = CONFIG.ROLL_CENTER
    DECAY_FACTOR = 4
    minDt = 0.01
    control_no_faces_counter = 0

    REAPPEAR_DISTANCE = 100.0
    DISAPPEAR_THRESHOLD_TIME = 0.0
    REAPPEAR_THRESHOLD_TIME = 0.25
    LOG_SIZE = 50
    STILL_DISTANCE_THRESHOLD = 50.0
    UNSTILL_DISTANCE_THRESHOLD = 50.0
    WAIT_TIME_THRESHOLD = 10.0
    MEDIUM_CHEER_DURATION = 1.5
    LONG_CHEER_DURATION = 3.0
    CHEER_MOVEMENT = 20.0
    
    question_start_time = None
    question_initial_delay = 3.0
    question_repeat_delay = 4.0 #seconds
    question_decay = .6
    question_phrase = question
    question_counter = 0
    question_last_play_time = None

    def __init__(self, brain):

        #FSM: initialize state machine
        self.machine = Machine(model=self, states=self.states, transitions=self.transitions, initial=FOLLOWING_STATE)

        self.lastSendTime = 0
        self.trackedFace = None

        self.face_distance_log = []
        self.last_logged_time = 0
        self.log_interval = 0.01

        self.wait_duration = None
        self.cheer_start_time = None
        self.cheer_duration = None

        self.brain_time = 0

        self.disappear_start_time = None
        self.reappear_start_time = None

        self.last_tracked_face = None

        self.active = False

        #subscribe to getting the faces messages from the brain
        brain.subscribe('faces', self.onFaces)
        return
    

    def activate(self, brain):
        #print "### PEEK_A_BOO ACTIVATED ###"
        self.active = True
        self.yawValue = brain.targetState.head[0]
        self.pitchValue = brain.targetState.head[1]

    def deactivate(self, brain):
        #print "### PEEK_A_BOO DEACTIVATED ###"
        self.active = False
        self.to_following()

    loopcounter = 0
    last_loop_time = 0
    def loop(self, brain, time):
        #dont run this loop excessively
        if (time - self.lastSendTime < self.minDt):
            return
        self.loopcounter+=1
        if self.loopcounter % 1000==0:
            dt = time - self.last_loop_time
            self.last_loop_time = time
          
            print 'Rate: ', self.loopcounter/dt
            self.loopcounter=0 

        self.lastSendTime = time
        self.brain_time = time
        
        if not self.active:
            return

        if time - self.last_logged_time >= self.log_interval:
            self.log_tracked_face()
            self.last_logged_time = time
            
            #print "[" + str(time) + "] logged new face:"
            #for i in range(0, len(self.face_distance_log)):
            #    print str(i) + ": " + str(self.face_distance_log[i])
            #
            #print "[" + str(time) + "] state: " + self.state

        #FSM: transition state if applicable given conditions
        self.transition_state(brain_time=time, the_brain=brain) 

        #FSM: execute run loop operations based on state
        if self.state == LOOKING_STATE:
            # example of speech that needs to be played on a loop
            #if brain.speech.isPhraseComplete:
            #    brain.speech.activatePhrase(cmajorbounce,time)
            brain.targetState.body = (0,255,255,0)
            self.face_distance_log = []
            brain.speech.quiet()

        elif self.state == FOLLOWING_STATE:

            still_ratio = self.stillness()
            brain.targetState.body = (0, 255. * still_ratio, 255. * (1. - still_ratio), 0)

            self.follow_face(brain)
             
        elif self.state == ENGAGING_STATE:

            unstill_ratio = self.unstillness()
            brain.targetState.body = (0, 255. * (1. - unstill_ratio), 255. * unstill_ratio, 0)

            self.follow_face(brain)

            if self.trackedFace is not None:
                self.last_tracked_face = self.trackedFace
        
        elif self.state == WAITING_STATE:
            if self.question_start_time == None:
                self.question_start_time = time
            self.question_elapsed_time = time - self.question_start_time
            brain.targetState.body = (255,0,255,0)
            
            #audio
            if self.question_elapsed_time > self.question_initial_delay and self.question_counter < 5:
                if self.question_last_play_time is None: #first play
                    brain.speech.activatePhrase(self.question_phrase,time)
                    self.question_counter+=1
                    self.question_last_play_time = time
                elif time-self.question_last_play_time > self.question_repeat_delay:
                    brain.speech.activatePhrase(self.question_phrase,time)
                    self.question_counter+=1
                    self.question_last_play_time = time
                    if self.question_repeat_delay > .5:
                        self.question_repeat_delay *= self.question_decay 
                    elif self.question_phrase.wordArray[0].duration > 10:
                        self.question_phrase = self.question_phrase.offsetDuration(0.85)
                    
        
        elif self.state == CHEERING_STATE:
           

            if self.cheer_start_time is None:

                self.cheer_start_time = time

                # set cheer duration based on wait duration
                if self.wait_duration < self.MEDIUM_CHEER_DURATION:  

                    # short cheer
                    self.cheer_duration = 0.25
                    brain.speech.activatePhrase(laughSoft,time)

                elif self.wait_duration < self.LONG_CHEER_DURATION:

                    # medium cheer
                    self.cheer_duration = 1.0
                    brain.speech.activatePhrase(laughMedium,time)
                else:

                    # long cheer
                    self.cheer_duration = 2.0
                    brain.speech.activatePhrase(laughHard,time)


            elif time - self.cheer_start_time <= self.cheer_duration:

                cheer_array = [
                    (255,255,0,0),
                    (0,255,255,0),
                    (255,0,255,0),
                    (255,255,255,0),
                    (255,255,0,0),
                    (0,255,255,0),
                    (255,0,255,0),
                    (255,255,255,0),
                    (255,255,0,0),
                    (0,255,255,0)
                ]
                cheer_index = int(round((time - self.cheer_start_time) * 10.0)) % 10
                brain.targetState.body = cheer_array[cheer_index]
                self.rollValue = CONFIG.ROLL_CENTER + (self.CHEER_MOVEMENT if cheer_index % 2 == 0 else -self.CHEER_MOVEMENT)
                brain.targetState.head = (self.yawValue, self.pitchValue, self.rollValue)

            elif time - self.cheer_start_time > self.cheer_duration:
                self.cheer_start_time = None
                brain.speech.activatePhrase(None,time)
        else:

            print "Uh oh, spaghettios."

    def follow_face(self, brain):

        if self.trackedFace is not None:

            self.yawValue, self.pitchValue = self.trackFace(self.trackedFace.getCentroid(), self.yawValue, self.pitchValue)
            brain.targetState.head = (self.yawValue, self.pitchValue, self.rollValue)

    def is_tracking_face(self):

        return self.trackedFace is not None

    def is_tracked_face_still(self):

        return (None not in self.face_distance_log and
                sum(numpy.array(self.face_distance_log) <= self.STILL_DISTANCE_THRESHOLD) == self.LOG_SIZE)

    def is_tracked_face_unstill(self):

        return (None not in self.face_distance_log and 
                sum(numpy.array(self.face_distance_log) > self.UNSTILL_DISTANCE_THRESHOLD) == self.LOG_SIZE)

    def stillness(self):

        filtered_face_log = [x for x in self.face_distance_log if x is not None]

        if len(filtered_face_log) == 0:
            return 0.0

        ratio = float(sum(numpy.array(filtered_face_log) <= self.STILL_DISTANCE_THRESHOLD)) / self.LOG_SIZE

        return ratio

    def unstillness(self):

        filtered_face_log = [x for x in self.face_distance_log if x is not None]

        if len(filtered_face_log) == 0:
            return 0.0

        ratio = float(sum(numpy.array(self.face_distance_log) > self.UNSTILL_DISTANCE_THRESHOLD)) / self.LOG_SIZE

        return ratio

    def did_face_disappear(self):

        if self.trackedFace is None:

            if self.disappear_start_time is None:

                # start disappearance threshold timer
                self.disappear_start_time = self.brain_time
                return False

            elif self.brain_time - self.disappear_start_time < self.DISAPPEAR_THRESHOLD_TIME:

                # no-op, still within disappearance threshold
                return False

            else:

                # consider face as disappeared and reset timer
                self.disappear_start_time = None
                return True

        else:

            # face is tracked, reset timer
            self.disappear_start_time = None
            return False

    def start_waiting(self, the_brain, brain_time=0):

        self.wait_duration = brain_time

    def did_face_reappear(self):

        #if self.trackedFace is not None:
        #    print "Reappearing face distance =", (self.trackedFace.getCentroid()).distance(self.last_tracked_face.getCentroid())

        if self.trackedFace is not None:

            if self.reappear_start_time is None:

                # start reappearance threshold timer
                self.reappear_start_time = self.brain_time
                return False

            elif self.brain_time - self.reappear_start_time < self.REAPPEAR_THRESHOLD_TIME:

                # no-op, still within reappearance threshold
                return False

            elif (self.trackedFace.getCentroid()).distance(self.last_tracked_face.getCentroid()) > self.REAPPEAR_DISTANCE:
                print 'reappeared face is too far away', self.trackedFace.getCentroid().distance(self.last_tracked_face.getCentroid())
                # reappeared face is too far away
                return False

            else:

                # consider face as reappeared and reset timer
                self.reappear_start_time = None
                return True

        else:

            # no face, reset timer
            self.reappear_start_time = None
            return False

    def waiting_too_long(self):

        return self.brain_time - self.wait_duration > self.WAIT_TIME_THRESHOLD

    def stop_waiting(self, the_brain, brain_time=0):

        self.wait_duration = round(brain_time - self.wait_duration)
        self.question_start_time = None
        self.question_counter = 0
        self.question_phrase = question
        self.face_distance_log = []
        self.question_start_time = None
        self.question_last_play_time = None
        self.question_initial_delay = 4.0
        self.question_repeat_delay = 4.0 

    def complain(self, the_brain, brain_time=0):

        the_brain.speech.activatePhrase(aminorTriad,brain_time)

    def is_done_cheering(self):

        return self.cheer_start_time is None

    def log_tracked_face(self):
        if self.state == LOOKING_STATE or self.state == CHEERING_STATE:
            return    
        face_distance = None
        if self.trackedFace is not None:
            face_distance = Point(FACE_CENTER_X, FACE_CENTER_Y).distance(self.trackedFace.getCentroid())

        self.face_distance_log.insert(0, face_distance)
        if len(self.face_distance_log) > self.LOG_SIZE:
            self.face_distance_log.pop()

    def trackFace(self, face, yawValue, pitchValue):
        # this is a very basic proportional gain based on center offset
        # in yaw, if the target x is > center, move left
    
        yawError = math.fabs(face.x - FACE_CENTER_X)
        pitchError = math.fabs(face.y - FACE_CENTER_Y)

        #our simple control law is just motion proportional to the error
        #of the current target in pixels. There is a dead zone to prevent 
        #noise from dominating the signal when we're close to the target.
        decay = (1.0/((self.control_no_faces_counter+1.0) * self.DECAY_FACTOR))
        if (yawError > self.deadZone):
            if (face.x > FACE_CENTER_X):
                yawValue -= ((self.yawEffort * yawError) * decay)
            if (face.x < FACE_CENTER_X):
                yawValue += ((self.yawEffort * yawError) * decay)
    
        if (pitchError > self.deadZone):
            if (face.y >FACE_CENTER_Y):
                pitchValue -= ((self.pitchEffort * pitchError) * decay)
            if (face.y < FACE_CENTER_Y):
                pitchValue += ((self.pitchEffort * pitchError) * decay)
    
        #prevent windup outside the physical limits
        if (yawValue < CONFIG.YAW_MIN):
            yawValue = CONFIG.YAW_MIN
        if (yawValue > CONFIG.YAW_MAX):
            yawValue = CONFIG.YAW_MAX
        if (pitchValue < CONFIG.PITCH_MIN):
            pitchValue = CONFIG.PITCH_MIN
        if (pitchValue > CONFIG.PITCH_MAX):
            pitchValue = CONFIG.PITCH_MAX

        self.control_no_faces_counter += 1
        return (yawValue, pitchValue)


    def onFaces(self, facesMsg):        
        #get the face closest to the center and set that to be the one we track
        center = Point(FACE_CENTER_X, FACE_CENTER_Y)
        minDist = 10000; # a big number
        ctrFace = None
        for faceMsg in facesMsg["faces"]:
            face = Face(
                Point(faceMsg["topleft"]["x"],faceMsg["topleft"]["y"]),
                Point(faceMsg["bottomright"]["x"],faceMsg["bottomright"]["y"]))
            dist = center.distance(face.getCentroid())
            if (dist < minDist):
                minDist = dist
                ctrFace = face;
        
            
        self.trackedFace = ctrFace
        self.control_no_faces_counter = 0
        
        

        

