#these define frequenies of common notes for use in the speech service.

NOTE_B0 = 31
NOTE_C1 = 33
NOTE_CS1 = 35
NOTE_D1 = 37
NOTE_DS1 = 39
NOTE_E1 = 41
NOTE_F1 = 44
NOTE_FS1 = 46
NOTE_G1 = 49
NOTE_GS1 = 52
NOTE_A1 = 55
NOTE_AS1 = 58
NOTE_B1 = 62
NOTE_C2 = 65
NOTE_CS2 = 69
NOTE_D2 = 73
NOTE_DS2 = 78
NOTE_E2 = 82
NOTE_F2 = 87
NOTE_FS2 = 93
NOTE_G2 = 98
NOTE_GS2 = 104
NOTE_A2 = 110
NOTE_AS2 = 117
NOTE_B2 = 123
NOTE_C3 = 131
NOTE_CS3 = 139
NOTE_D3 = 147
NOTE_DS3 = 156
NOTE_E3 = 165
NOTE_F3 = 175
NOTE_FS3 = 185
NOTE_G3 = 196
NOTE_GS3 = 208
NOTE_A3 = 220
NOTE_AS3 = 233
NOTE_B3 = 247
NOTE_C4 = 262
NOTE_CS4 = 277
NOTE_D4 = 294
NOTE_DS4 = 311
NOTE_E4 = 330
NOTE_F4 = 349
NOTE_FS4 = 370
NOTE_G4 = 392
NOTE_GS4 = 415
NOTE_A4 = 440
NOTE_AS4 = 466
NOTE_B4 = 494
NOTE_C5 = 523
NOTE_CS5 = 554
NOTE_D5 = 587
NOTE_DS5 = 622
NOTE_E5 = 659
NOTE_F5 = 698
NOTE_FS5 = 740
NOTE_G5 = 784
NOTE_GS5 = 831
NOTE_A5 = 880
NOTE_AS5 = 932
NOTE_B5 = 988
NOTE_C6 = 1047
NOTE_CS6 = 1109
NOTE_D6 = 1175
NOTE_DS6 = 1245
NOTE_E6 = 1319
NOTE_F6 = 1397
NOTE_FS6 = 1480
NOTE_G6 = 1568
NOTE_GS6 = 1661
NOTE_A6 = 1760
NOTE_AS6 = 1865
NOTE_B6 = 1976
NOTE_C7 = 2093
NOTE_CS7 = 2217
NOTE_D7 = 2349
NOTE_DS7 = 2489
NOTE_E7 = 2637
NOTE_F7 = 2794
NOTE_FS7 = 2960
NOTE_G7 = 3136
NOTE_GS7 = 3322
NOTE_A7 = 3520
NOTE_AS7 = 3729
NOTE_B7 = 3951
NOTE_C8 = 4186
NOTE_CS8 = 4435
NOTE_D8 = 4699
NOTE_DS8 = 4978

def autotune(x):     
    if(x <= 32): x = 31
    if( x > 32 and x<= 34): x= 33
    if( x > 34 and x<= 36): x= 35
    if( x > 36 and x<= 38): x= 37
    if( x > 38 and x<= 40): x= 39
    if( x > 40 and x<= 42.5): x= 41
    if( x > 42.5 and x<= 45): x= 44
    if( x > 45 and x<= 47.5): x= 46
    if( x > 47.5 and x<= 50.5): x= 49
    if( x > 50.5 and x<= 53.5): x= 52
    if( x > 53.5 and x<= 56.5): x= 55
    if( x > 56.5 and x<= 60): x= 58
    if( x > 60 and x<= 63.5): x= 62
    if( x > 63.5 and x<= 67): x= 65
    if( x > 67 and x<= 71): x= 69
    if( x > 71 and x<= 75.5): x= 73
    if( x > 75.5 and x<= 80): x= 78
    if( x > 80 and x<= 84.5): x= 82
    if( x > 84.5 and x<= 90): x= 87
    if( x > 90 and x<= 95.5): x= 93
    if( x > 95.5 and x<= 101): x= 98
    if( x > 101 and x<= 107): x= 104
    if( x > 107 and x<= 113.5): x= 110
    if( x > 113.5 and x<= 120): x= 117
    if( x > 120 and x<= 127): x= 123
    if( x > 127 and x<= 135): x= 131
    if( x > 135 and x<= 143): x= 139
    if( x > 143 and x<= 151.5): x= 147
    if( x > 151.5 and x<= 160.5): x= 156
    if( x > 160.5 and x<= 170): x= 165
    if( x > 170 and x<= 180): x= 175
    if( x > 180 and x<= 190.5): x= 185
    if( x > 190.5 and x<= 202): x= 196
    if( x > 202 and x<= 214): x= 208
    if( x > 214 and x<= 226.5): x= 220
    if( x > 226.5 and x<= 240): x= 233
    if( x > 240 and x<= 254.5): x= 247
    if( x > 254.5 and x<= 269.5): x= 262
    if( x > 269.5 and x<= 285.5): x= 277
    if( x > 285.5 and x<= 302.5): x= 294
    if( x > 302.5 and x<= 320.5): x= 311
    if( x > 320.5 and x<= 339.5): x= 330
    if( x > 339.5 and x<= 359.5): x= 349
    if( x > 359.5 and x<= 381): x= 370
    if( x > 381 and x<= 403.5): x= 392
    if( x > 403.5 and x<= 427.5): x= 415
    if( x > 427.5 and x<= 453): x= 440
    if( x > 453 and x<= 480): x= 466
    if( x > 480 and x<= 508.5): x= 494
    if( x > 508.5 and x<= 538.5): x= 523
    if( x > 538.5 and x<= 570.5): x= 554
    if( x > 570.5 and x<= 604.5): x= 587
    if( x > 604.5 and x<= 640.5): x= 622
    if( x > 640.5 and x<= 678.5): x= 659
    if( x > 678.5 and x<= 719): x= 698
    if( x > 719 and x<= 762): x= 740
    if( x > 762 and x<= 807.5): x= 784
    if( x > 807.5 and x<= 855.5): x= 831
    if( x > 855.5 and x<= 906): x= 880
    if( x > 906 and x<= 960): x= 932
    if( x > 960 and x<= 1017.5): x= 988
    if( x > 1017.5 and x<= 1078): x= 1047
    if( x > 1078 and x<= 1142): x= 1109
    if( x > 1142 and x<= 1210): x= 1175
    if( x > 1210 and x<= 1282): x= 1245
    if( x > 1282 and x<= 1358): x= 1319
    if( x > 1358 and x<= 1438.5): x= 1397
    if( x > 1438.5 and x<= 1524): x= 1480
    if( x > 1524 and x<= 1614.5): x= 1568
    if( x > 1614.5 and x<= 1710.5): x= 1661
    if( x > 1710.5 and x<= 1812.5): x= 1760
    if( x > 1812.5 and x<= 1920.5): x= 1865
    if( x > 1920.5 and x<= 2034.5): x= 1976
    if( x > 2034.5 and x<= 2155): x= 2093
    if( x > 2155 and x<= 2283): x= 2217
    if( x > 2283 and x<= 2419): x= 2349
    if( x > 2419 and x<= 2563): x= 2489
    if( x > 2563 and x<= 2715.5): x= 2637
    if( x > 2715.5 and x<= 2877): x= 2794
    if( x > 2877 and x<= 3048): x= 2960
    if( x > 3048 and x<= 3229): x= 3136
    if( x > 3229 and x<= 3421): x= 3322
    if( x > 3421 and x<= 3624.5): x= 3520
    if( x > 3624.5 and x<= 3840): x= 3729
    if( x > 3840 and x<= 4068.5): x= 3951
    if( x > 4068.5 and x<= 4310.5): x= 4186
    if( x > 4310.5 and x<= 4567): x= 4435
    if( x > 4567 and x<= 4838.5): x= 4699
    if( x > 4838.5): x= 4978
    return x
