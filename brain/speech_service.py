import math
from tsconstants import *
from speech_phrases import *

class SpeechService:
    def __init__(self, brain):
        scaledir = 50
        # pitch, vol, duration
        self.phrase = None
        self.wordStartTime = 0.0        
        self.phraseIndex = 0  
        self.isPhraseComplete = True  
        self.brain = None 
        return

    def activatePhrase(self, phrase, time):
        self.phrase = phrase
        self.phraseIndex = 0
        self.wordStartTime = time    
        self.isPhraseComplete = False   

    def quiet(self):
        if self.brain == None:  
            return
        self.brain.targetState.sound = (SOUND_OFF, 10, 0)
        self.phraseIndex = 0
        self.wordStartTime = 0.0    
        self.isPhraseComplete = True

    def loop(self, brain, time): 
        if self.brain == None:  
            self.brain = brain
                
        if (self.phrase == None):
            self.isPhraseComplete = True
            self.phraseIndex = 0
            self.wordStartTime = time    
            return

        #this function is called at an interval of time
        dt = time - self.wordStartTime
        dtMs = dt * 1000; #alonso's timing is in ms 
        currentWord = self.phrase.wordArray[self.phraseIndex]
        
        if (dtMs >= currentWord.duration):
            self.phraseIndex+=1
            dt = 0
            dtMs = 0
            if (self.phraseIndex >= len(self.phrase.wordArray)):
                self.phrase = None
                self.phraseIndex = 0;
                brain.targetState.sound = (SOUND_OFF, 10, 0)
                print 'finished phrase!'
                self.isPhraseComplete = True   
                return
            currentWord = self.phrase.wordArray[self.phraseIndex]            
            self.wordStartTime = time
            

        #now play the word      
        wordPercent = (dtMs) / float(currentWord.duration) if currentWord.duration!=0 else 0
        curPitch = wordPercent * currentWord.end_pitch + (1 - wordPercent) * currentWord.start_pitch
        if (curPitch == 0):
            brain.targetState.sound = (SOUND_OFF, 10, 0)
        else:
            if (self.phrase.logarithmic):
                curPitch = 40 * math.pow(2.0, curPitch/108.0);
            curPitch = autotune(curPitch)
            brain.targetState.sound = (SOUND_ON, 10, curPitch)
        return