import json, math

from tscommon import Point, Face
from tsconstants import *

import random
from transitions import Machine

#FSM: state label constants
LOOKING_STATE = 'looking'
MOVING_STATE = 'moving'
FOLLOWING_STATE = 'following'

class LookAroundBehavior(object):
    """Performs simple face tracking using input from the c++ facedetect"""

   #FSM: define states 
    states = [
        LOOKING_STATE, 
        MOVING_STATE, 
        FOLLOWING_STATE
    ]

    #FSM: define triggers to transition between states, including conditional checks
    transitions = [
        { 'trigger': 'transition_state', 'source': LOOKING_STATE, 'dest': MOVING_STATE, 'conditions': 'is_done_looking', 'after': 'start_moving' },
        { 'trigger': 'transition_state', 'source': LOOKING_STATE, 'dest': FOLLOWING_STATE, 'conditions': 'did_find_face' },
        { 'trigger': 'transition_state', 'source': MOVING_STATE, 'dest': LOOKING_STATE, 'conditions': 'is_done_moving', 'after': 'start_looking' },
        { 'trigger': 'transition_state', 'source': MOVING_STATE, 'dest': FOLLOWING_STATE, 'conditions': 'did_find_face' },
        { 'trigger': 'transition_state', 'source': FOLLOWING_STATE, 'dest': LOOKING_STATE, 'unless': 'did_find_face', 'after': 'start_looking' }
    ]

    deadZone = 2
    yawEffort = -0.8
    pitchEffort = -0.5
    yawValue = CONFIG.YAW_CENTER
    pitchValue = CONFIG.PITCH_CENTER
    minDt = 0.01
    PITCH_MAX_OFFSET = 100
    DECAY_FACTOR = 4
    control_no_faces_counter = 0

    def __init__(self, brain):

        #FSM: initialize state machine
        self.machine = Machine(model=self, states=self.states, transitions=self.transitions, initial=LOOKING_STATE)

        self.lastSendTime = 0
        self.trackedFace = None

        self.brain_time = 0
        self.look_start_time = 0.
        self.look_duration = 0.

        self.move_step = (0, 0, 0) 
        self.move_start_time = 0.
        self.move_duration = 0.

        self.active = False

        #subscribe to getting the faces messages from the brain
        brain.subscribe('faces', self.onFaces)
        return

    def activate(self, brain):
        #print "### LOOK_AROUND ACTIVATED ###"
        self.active = True
        self.yawValue = brain.targetState.head[0]
        self.pitchValue = brain.targetState.head[1]

    def deactivate(self, brain):
        #print "### LOOK_AROUND DEACTIVATED ###"
        self.active = False
        self.to_looking()

    def loop(self, brain, time):
        #dont run this loop excessively
        if (time - self.lastSendTime < self.minDt):
            return
        self.lastSendTime = time

        self.brain_time = time    
        #1print "[" + str(time) + "] state: " + self.state

        if not self.active:
            return

        #FSM: transition state if applicable given conditions
        self.transition_state() 

        #FSM: execute run loop operations based on state
        if self.state == LOOKING_STATE:
            
            brain.targetState.body = (0,255,255,0)

        elif self.state == MOVING_STATE:

            brain.targetState.body = (0,255,255,0)
            self.move_head(brain)

        elif self.state == FOLLOWING_STATE:

            brain.targetState.body = (0,255,0,0)
            self.follow_face(brain)

        else:

            print "Uh oh, spaghettios."

    def start_looking(self):

        self.look_start_time = self.brain_time
        self.look_duration = random.uniform(1.0, 4.0)

    def is_done_looking(self):

        return self.brain_time - self.look_start_time >= self.look_duration

    def start_moving(self):

        self.move_start_time = self.brain_time

        # determine which quadrant currently looking at
        current_quadrant = 0        
        if CONFIG.YAW_CENTER <= self.yawValue <= CONFIG.YAW_MAX and CONFIG.PITCH_CENTER <= self.pitchValue <= CONFIG.PITCH_MAX:
        
            current_quadrant = 1

        elif CONFIG.YAW_MIN <= self.yawValue <= CONFIG.YAW_CENTER and CONFIG.PITCH_CENTER <= self.pitchValue <= CONFIG.PITCH_MAX:

            current_quadrant = 2

        elif CONFIG.YAW_MIN <= self.yawValue <= CONFIG.YAW_CENTER and CONFIG.PITCH_MIN <= self.pitchValue <= CONFIG.PITCH_CENTER:

            current_quadrant = 3

        elif CONFIG.YAW_CENTER <= self.yawValue <= CONFIG.YAW_MAX and CONFIG.PITCH_MIN <= self.pitchValue <= CONFIG.PITCH_CENTER:

            current_quadrant = 4

        # determine which quadrant to go to
        new_quadrant = random.randint(1, 4)
        while new_quadrant == current_quadrant:

            new_quadrant = random.randint(1, 4)

        # determine what position in that quadrant to move to
        q_yaw_min = 0
        q_yaw_max = 0
        q_pitch_min = 0
        q_pitch_max = 0

        if new_quadrant == 1:

            q_yaw_min = CONFIG.YAW_CENTER
            q_yaw_max = CONFIG.YAW_MAX
            q_pitch_min = CONFIG.PITCH_CENTER
            q_pitch_max = CONFIG.PITCH_MAX - self.PITCH_MAX_OFFSET

        elif new_quadrant == 2:

            q_yaw_min = CONFIG.YAW_MIN
            q_yaw_max = CONFIG.YAW_CENTER
            q_pitch_min = CONFIG.PITCH_CENTER
            q_pitch_max = CONFIG.PITCH_MAX - self.PITCH_MAX_OFFSET

        elif new_quadrant == 3:

            q_yaw_min = CONFIG.YAW_MIN
            q_yaw_max = CONFIG.YAW_CENTER
            q_pitch_min = CONFIG.PITCH_MIN
            q_pitch_max = CONFIG.PITCH_CENTER

        elif new_quadrant == 4:

            q_yaw_min = CONFIG.YAW_CENTER
            q_yaw_max = CONFIG.YAW_MAX
            q_pitch_min = CONFIG.PITCH_MIN 
            q_pitch_max = CONFIG.PITCH_CENTER

        target_yaw_value = random.uniform(q_yaw_min, q_yaw_max)
        target_pitch_value = random.uniform(q_pitch_min, q_pitch_max)
        
        print "Moving from Quadrant", current_quadrant, (self.yawValue, self.pitchValue), "to Quadrant", new_quadrant, (target_yaw_value, target_pitch_value), "in", self.move_duration, "seconds"

        # determine what duration and step to set to get to that position
        r2_distance = math.sqrt((target_yaw_value - self.yawValue)**2 +  (target_pitch_value - self.pitchValue)**2)
        r2_distance_max = math.sqrt((CONFIG.YAW_MAX - CONFIG.YAW_MIN)**2 +  (CONFIG.PITCH_MAX - CONFIG.PITCH_MIN)**2)
        r2_percent = r2_distance/r2_distance_max
        self.move_duration = random.uniform(0.2,1.0)
        run_duration = 1.0/self.minDt
        number_runs = self.move_duration * run_duration

        yaw_step =  (target_yaw_value - self.yawValue)/number_runs
        pitch_step =  (target_pitch_value - self.pitchValue)/number_runs

        #clamp yawstep and pitchstep
        MAX_YAW_STEP = 10
        MAX_PITCH_STEP = 10
        if yaw_step < -MAX_YAW_STEP:
            yaw_step = -MAX_YAW_STEP
        if yaw_step > MAX_YAW_STEP:
            yaw_step = MAX_YAW_STEP
        if pitch_step < -MAX_PITCH_STEP:
            pitch_step = -MAX_PITCH_STEP
        if pitch_step > MAX_PITCH_STEP:
            pitch_step = MAX_PITCH_STEP
        #print yaw_step, pitch_step
        self.move_step = (yaw_step, pitch_step, 0)

    def is_done_moving(self):

        return self.brain_time - self.move_start_time >= self.move_duration

    def did_find_face(self):

        return self.trackedFace is not None

    def move_head(self, brain):

        self.yawValue += self.move_step[0]
        self.pitchValue += self.move_step[1]

        look_yaw_min = CONFIG.YAW_MIN * 1.0
        look_yaw_max = CONFIG.YAW_MAX * 1.0
        look_pitch_min = CONFIG.PITCH_MIN * 1.0
        look_pitch_max = CONFIG.PITCH_MAX * 1.0

        if (self.yawValue < look_yaw_min):
            self.yawValue = look_yaw_min
        if (self.yawValue > look_yaw_max):
            self.yawValue = look_yaw_max
        if (self.pitchValue < look_pitch_min):
            self.pitchValue = look_pitch_min
        if (self.pitchValue > look_pitch_max):
            self.pitchValue = look_pitch_max

        brain.targetState.head = (self.yawValue, self.pitchValue, CONFIG.ROLL_CENTER)

    def follow_face(self, brain):

        if self.trackedFace is not None:

            self.yawValue, self.pitchValue = self.trackFace(self.trackedFace.getCentroid(), self.yawValue, self.pitchValue)
            brain.targetState.head = (self.yawValue, self.pitchValue, CONFIG.ROLL_CENTER)

    def trackFace(self, face, yawValue, pitchValue):
       # this is a very basic proportional gain based on center offset
        # in yaw, if the target x is > center, move left
    
        yawError = math.fabs(face.x - FACE_CENTER_X)
        pitchError = math.fabs(face.y - FACE_CENTER_Y)

        #our simple control law is just motion proportional to the error
        #of the current target in pixels. There is a dead zone to prevent 
        #noise from dominating the signal when we're close to the target.
        decay = (1.0/((self.control_no_faces_counter+1.0) * self.DECAY_FACTOR))
        if (yawError > self.deadZone):
            if (face.x > FACE_CENTER_X):
                yawValue -= ((self.yawEffort * yawError) * decay)
            if (face.x < FACE_CENTER_X):
                yawValue += ((self.yawEffort * yawError) * decay)
    
        if (pitchError > self.deadZone):
            if (face.y >FACE_CENTER_Y):
                pitchValue -= ((self.pitchEffort * pitchError) * decay)
            if (face.y < FACE_CENTER_Y):
                pitchValue += ((self.pitchEffort * pitchError) * decay)
    
        #prevent windup outside the physical limits
        if (yawValue < CONFIG.YAW_MIN):
            yawValue = CONFIG.YAW_MIN
        if (yawValue > CONFIG.YAW_MAX):
            yawValue = CONFIG.YAW_MAX
        if (pitchValue < CONFIG.PITCH_MIN):
            pitchValue = CONFIG.PITCH_MIN
        if (pitchValue > CONFIG.PITCH_MAX):
            pitchValue = CONFIG.PITCH_MAX

        self.control_no_faces_counter += 1
        return (yawValue, pitchValue)


    def onFaces(self, facesMsg):        
        #get the face closest to the center and set that to be the one we track
        center = Point(FACE_CENTER_X, FACE_CENTER_Y)
        minDist = 10000; # a big number
        ctrFace = None
        for faceMsg in facesMsg["faces"]:
            face = Face(
                Point(faceMsg["topleft"]["x"],faceMsg["topleft"]["y"]),
                Point(faceMsg["bottomright"]["x"],faceMsg["bottomright"]["y"]))
            dist = center.distance(face.getCentroid())
            if (dist < minDist):
                minDist = dist
                ctrFace = face;
        
            
        self.trackedFace = ctrFace
        self.control_no_faces_counter = 0
        
        

        

