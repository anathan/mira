import sys
sys.path.append('../common')
sys.path.append('../common/network')

import math, json, time
import tsconstants
import tsnetwork

import look_around_behavior
from look_around_behavior import LookAroundBehavior

import peek_a_boo_behavior
from peek_a_boo_behavior import PeekABooBehavior

from speech_service import SpeechService
from control_service import ControlService
from tsconstants import *

class MiraState:
    def __init__(self):
        self.head = (CONFIG.YAW_CENTER, CONFIG.PITCH_CENTER, CONFIG.ROLL_CENTER )
        self.body = (0, 0, 0, 0 ) #RGBI
        self.sound = (SOUND_OFF, SOUND_VOLUME_DEFAULT, 0 )
        self.breathing_period = 3.0 #seconds

class MiraBrain:
    def __init__(self):
        #Register all things that are listening for UDP with the manager here.
        self.rxMgr  = tsnetwork.UdpManager()
        self.rxMgr.register(tsnetwork.UdpReceiver(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT, self._onFaces))

        #Create specific senders here and pass them to sub behaviors. 
        self.ctrlTx = tsnetwork.UdpSender(LISTEN_IP, CONTROLLER_IP, CONTROLLER_PORT)
        self.timeStart = time.time();
        
        self.subscriptions = dict([('faces',[]),('motion',[])])
        self.targetState = MiraState()
    
        self.look_around = LookAroundBehavior(self)
        self.peek_a_boo = PeekABooBehavior(self)

        self.active_behavior = self.look_around

        self.speech = SpeechService(self)
        self.control = ControlService(self)

        #anything that has a "loop" has to be listed here as a task
        self.tasks = [self.look_around, self.peek_a_boo, self.speech, self.control]

    #udp callback (internal)   
    def _onFaces(self, msg):
        faces = json.loads(msg)        
        for cb in self.subscriptions['faces']:
            cb(faces)

    #this is the main loop, calls each behavior and service's loop function
    def loop(self):
        tik = time.time() - self.timeStart

        #udp servicing
        self.rxMgr.loop()
        
        #tell each task to run one loop iteration (tasks being behaviors AND services)
        for t in self.tasks:

            if t is self.look_around:

                if self.active_behavior is not self.look_around or self.look_around.state == look_around_behavior.FOLLOWING_STATE:

                    self.active_behavior = self.peek_a_boo
                    self.peek_a_boo.activate(brain)
                    self.look_around.deactivate(brain)

            if t is self.peek_a_boo:

                if self.active_behavior is not self.peek_a_boo or self.peek_a_boo.state == peek_a_boo_behavior.LOOKING_STATE:

                    self.active_behavior = self.look_around
                    self.look_around.activate(brain)
                    self.peek_a_boo.deactivate(brain)

            t.loop(self, tik)


    #behvaiors or services can get messages that are sent to individual channels
    # and have the function 'cb' called when that happens
    def subscribe(self, type, cb):
        self.subscriptions.setdefault(type,[]).append(cb)
       


if mira_id() == -1:
    print 'EXITING SINCE ID COULD NOT BE DETERMINED. Please create a mira.txt file with the text MiraN where N is your mira id!'
    exit()
brain = MiraBrain()

while True:
    brain.loop()
    #release any extra cpu 
    time.sleep(0.001); 
    
    
    
