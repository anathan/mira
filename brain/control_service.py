import math, json, time, colorsys, numpy
from tsconstants import *

class ControlService:

    
    BREATHING_MAG = 0.175
    BREATHING_BIAS = 1.0 - BREATHING_MAG

    breathing_cycle_start = 0
    breathing_cycle_elapsed = 0
    def __init__(self, brain):
        self.minDt = .03 #33hz
        self.lastSendTime = 0        
        return

    def loop(self, brain, time):
        if (time - self.lastSendTime < self.minDt):
            return
        
        #breathing controller--------------
        self.breathing_cycle_elapsed = time - self.breathing_cycle_start
        if self.breathing_cycle_elapsed >= brain.targetState.breathing_period:
            self.breathing_cycle_elapsed = 0
            self.breathing_cycle_start = time

        #convert to HLS color space and make the sin travel in lumanance
        hb_mag = self.BREATHING_MAG * math.sin(self.breathing_cycle_elapsed / brain.targetState.breathing_period * 2.0 * math.pi) + self.BREATHING_BIAS
        #print hb_mag
        (h,l,s) = colorsys.rgb_to_hls(brain.targetState.body[0] /255.0,brain.targetState.body[1] /255.0,brain.targetState.body[2] /255.0)
        l *= hb_mag
        (rCommand, gCommand, bCommand) = colorsys.hls_to_rgb(h,l,s)
        #rescale
        rCommand *= 255.0
        gCommand *= 255.0
        bCommand *= 255.0
        
        #roll/pitch mixer controller--------------
        #we want to "mix" pitch and roll such that pitch is 100% at yaw = 0, and roll is 100% at yaw = 90
        #we also want roll to be around the effective axis of the yaw-> pitch arm
        yInput =brain.targetState.head[0]
        pInput =brain.targetState.head[1]
        rInput = brain.targetState.head[2]

        yawRadians = math.pi / 180.0 * DEGREE_PER_TICK * (yInput-CONFIG.YAW_CENTER);
        rDelta = rInput - CONFIG.ROLL_CENTER;
        pDelta = pInput - CONFIG.PITCH_CENTER;
      
        yawCommand = yInput
        pitchCommand = int(CONFIG.PITCH_CENTER + (math.cos(yawRadians) * pDelta) - (math.sin(yawRadians) * rDelta));
        rollCommand = int(CONFIG.ROLL_CENTER - (math.sin(yawRadians) * pDelta) - (math.cos(yawRadians) * rDelta));
        
        #gating
        yawCommand = numpy.clip(yawCommand, CONFIG.YAW_MIN, CONFIG.YAW_MAX)
        pitchCommand = numpy.clip(pitchCommand, CONFIG.PITCH_MIN, CONFIG.PITCH_MAX)
        rollCommand = numpy.clip(rollCommand, CONFIG.ROLL_MIN, CONFIG.ROLL_MAX)
        rCommand = numpy.clip(rCommand, 0, 255.0)
        gCommand = numpy.clip(gCommand, 0, 255.0)
        bCommand = numpy.clip(bCommand, 0, 255.0)

        #print "Y", yInput, yawCommand, "P", pInput, pitchCommand, "R", rInput, rollCommand
        ctrlMsg = json.dumps({ 
            "head": { 
                "yaw": int(yawCommand), 
                "pitch": int(pitchCommand), 
                "roll": int(rollCommand)
            },
            "sound": { 
                "mode": int(brain.targetState.sound[0]), 
                "vol": int(brain.targetState.sound[1]), 
                "freq": int(brain.targetState.sound[2])
            },
            "body":{
                "r": int(rCommand), 
                "g": int(gCommand), 
                "b": int(bCommand),
                "i": int(brain.targetState.body[3])
            },
            "eye":{
                "left": int(0), 
                "right": int(0)
            }
        })

        brain.ctrlTx.send(ctrlMsg)
        self.lastSendTime = time;
        return