import json, math

from tscommon import Point, Face
from tsconstants import *

class TrackBehavior(object):
    """Performs simple face tracking using input from the c++ facedetect"""

    deadZone = 2
    yawEffort = .01
    pitchEffort = -0.005
    yawValue = YAW_CENTER
    pitchValue = PITCH_CENTER
    minDt = 0.01

    def __init__(self, brain):
        self.lastSendTime = 0
        self.trackedFace = None

        #subscribe to getting the faces messages from the brain
        brain.subscribe('faces', self.onFaces)
        return

    def loop(self, brain, time):
        #dont run this loop excessively
        if (time - self.lastSendTime < self.minDt):
            return
        if (self.trackedFace != None):
            self.yawValue, self.pitchValue = self.trackFace(self.trackedFace.getCentroid(), self.yawValue, self.pitchValue)
            brain.targetState.head = (self.yawValue, self.pitchValue, ROLL_CENTER)
            brain.targetState.body = (0,255,0,0)
        else:
            brain.targetState.body = (255,0,0,0)

    def trackFace(self, face, yawValue, pitchValue):
        # this is a very basic proportional gain based on center offset
        # in yaw, if the target x is > center, move left
    
        yawError = math.fabs(face.x - FACE_CENTER_X)
        pitchError = math.fabs(face.y - FACE_CENTER_Y)

        #our simple control law is just motion proportional to the error
        #of the current target in pixels. There is a dead zone to prevent 
        #noise from dominating the signal when we're close to the target.
        if (yawError > self.deadZone):
            if (face.x > FACE_CENTER_X):
                yawValue -= (self.yawEffort * yawError)
            if (face.x < FACE_CENTER_X):
                yawValue += (self.yawEffort * yawError)
    
        if (pitchError > self.deadZone):
            if (face.y >FACE_CENTER_Y):
                pitchValue -= (self.pitchEffort * pitchError)
            if (face.y < FACE_CENTER_Y):
                pitchValue += (self.pitchEffort * pitchError)
    
        #prevent windup outside the physical limits
        if (yawValue < YAW_MIN):
            yawValue = YAW_MIN
        if (yawValue > YAW_MAX):
            yawValue = YAW_MAX
        if (pitchValue < PITCH_MIN):
            pitchValue = PITCH_MIN
        if (pitchValue > PITCH_MAX):
            pitchValue = PITCH_MAX
        return (yawValue, pitchValue)


    def onFaces(self, facesMsg):        
        #get the face closest to the center and set that to be the one we track
        center = Point(FACE_CENTER_X, FACE_CENTER_Y)
        minDist = 10000; # a big number
        ctrFace = None
        for faceMsg in facesMsg["faces"]:
            face = Face(
                Point(faceMsg["topleft"]["x"],faceMsg["topleft"]["y"]),
                Point(faceMsg["bottomright"]["x"],faceMsg["bottomright"]["y"]))
            dist = center.distance(face.getCentroid())
            if (dist < minDist):
                minDist = dist
                ctrFace = face;
        
            
        self.trackedFace = ctrFace
        
        

        

