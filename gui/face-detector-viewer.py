import sys
sys.path.append('../brain')
sys.path.append('../common/network')

import json
import tsnetwork, select
from tsconstants import *
from GUI import Task, Window, View, Menu, application, run
from GUI.StdColors import black, white
def test(msg):

    print "F: " + msg
        
    facesMsg = json.loads(msg)
    # HACK: assume first face is the one we care about
    if len(facesMsg["faces"]) > 0:        
        face.left = facesMsg["faces"][0]["topleft"]["x"]
        face.top = facesMsg["faces"][0]["topleft"]["y"]
        face.right = facesMsg["faces"][0]["bottomright"]["x"]
        face.bottom = facesMsg["faces"][0]["bottomright"]["y"]

manager  = tsnetwork.UdpManager()
tx = tsnetwork.UdpSender(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT)

manager.register(tsnetwork.UdpReceiver(LISTEN_IP, FACE_DETECT_IP, FACE_DETECT_PORT, test))

# check incoming face messages and update detector view
def updateView():
    
    manager.loop()
    view.faceRect = face.get_rect()
    view.invalidate()

# "model" for detected face frame
class DetectedFace(object):
    
    def __init__(self, left, top, right, bottom):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom
        
    def get_rect(self):
        return (self.left, self.top, self.right, self.bottom)

# view representing camera frame
class DetectorView(View):
    
    faceRect = (0, 0, 0, 0)
    
    def draw(self, c, r): 
        # HACK: assumes only one face
        c.forecolor = white
        c.fill_rect(r)
        c.forecolor = black        
        c.stroke_rect(self.faceRect)


view = DetectorView(size = (320, 240))
win = Window(title = "Mira Face Detector Viewer")


face = DetectedFace(0, 0, 0, 0)

# recurring task to check incoming face messages and update detector view
viewUpdater = Task(updateView, 1.0 / 30.0, repeat = True, start = False)
viewUpdater.start()

win.add(view)
win.shrink_wrap()
view.become_target()


win.show()
app = application()
app.run()

main()


