import sys
sys.path.append('../brain')
sys.path.append('../common/network')

import math, json, time
import tsconstants
import tsnetwork

from GUI import Task, Label, Slider, Row, Window, run
from tsconstants import *

ctrlTx = tsnetwork.UdpSender(LISTEN_IP, CONTROLLER_IP, CONTROLLER_PORT)

# our venerable slider controls (hackily doubling as the "model")
yawSlider = pitchSlider = rollSlider = None

def sendControlMessage():
    ctrlMsg = json.dumps({ 
        "head": { 
            "yaw": int(yawSlider.value), 
            "pitch":int(pitchSlider.value), 
            "roll":int(rollSlider.value) 
        }
    })
    ctrlTx.send(ctrlMsg)
    print "Sending: %s" % (ctrlMsg)
    #
    # send control message over the wire here
    #

# subclass window component to support keyboard commands
key_step_size = 1
class HeadControlWindow(Window):
    
    def key_down(self, event):
        
        if event.char == 'a':
            yawSlider.value -= key_step_size
            #sendControlMessage()
        elif event.char == 'd':
            yawSlider.value += key_step_size
            #sendControlMessage() 
        elif event.char == 's':
            pitchSlider.value -= key_step_size
            #sendControlMessage()
        elif event.char == 'w':
            pitchSlider.value += key_step_size
            #sendControlMessage()
        elif event.char == 'z':
            rollSlider.value -= key_step_size
            #sendControlMessage()
        elif event.char == 'c':
            rollSlider.value += key_step_size
            #sendControlMessage()
        else:
            pass # no-op

# set up app window
win = HeadControlWindow(title = "Mira Head Controller", position = (10,10),
                        auto_position = False)

# configure yaw, pitch and roll sliders
yawLabel = Label(text = "Yaw")
yawSlider = Slider(orient = 'h', min_value = YAW_MIN, max_value = YAW_MAX, 
                   value = YAW_CENTER)
yawSlider.action = sendControlMessage

pitchLabel = Label(text = "Pitch")
pitchSlider = Slider(orient = 'v', min_value = PITCH_MIN, 
                     max_value = PITCH_MAX, value = PITCH_CENTER)
#pitchSlider.action = sendControlMessage

rollLabel = Label(text = "Roll")
rollSlider = Slider(orient = 'h', min_value = ROLL_MIN, max_value = ROLL_MAX, 
                    value = ROLL_CENTER)
#rollSlider.action = sendControlMessage
rollSlider.hmove = True

# lay out and configure window
win.place_column([Row([yawLabel, yawSlider]), Row([pitchLabel, pitchSlider]), 
                 Row([rollLabel, rollSlider])], left = 20, top = 20, 
                 spacing = 20, sticky = 'ew')
win.shrink_wrap()
win.show()       

# set up recurring task to send control messages
controlMessageHeartbeat = Task(sendControlMessage, 1.0 / 30.0, repeat = True, 
                               start = False)
controlMessageHeartbeat.start()

# go time
run()
    
