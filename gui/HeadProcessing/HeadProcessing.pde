import hypermedia.net.*;

/*
This code sends MouseY position and mouse click events though serial. It also draws scrolling history to be able to visualise Mira speak
into frequencies. The xPosition of the mouse controls volume. The yPosition of the mouse controls pitch. Mouse clicks make sounds. Any keyboard
press + mouse click will go into recording mode and print out code required to recreate sounds. 
*/
UDP udp;       
int PORT_RX=41150;
String HOST_IP = "224.1.1.2";//IP Address of the PC in which this App is running

int WINDOW_SIZE = 800;

int YAW_MIN= 1000;
int YAW_MAX = 2000;
int YAW_CENTER = 1580;

int PITCH_MIN = 1000;
int PITCH_MAX = 1600;
int PITCH_CENTER = 1400;

int ROLL_MIN = 1000;
int ROLL_MAX = 2000;
int ROLL_CENTER = 1575;

int yPos = YAW_CENTER; 
int pPos = PITCH_CENTER; 
int rPos = ROLL_CENTER;

byte mouseClick =0;

boolean noteState = true;
boolean keyboardState = false;
boolean rollActive = false;
boolean yawActive = true;
boolean pitchActive = false;
boolean yawBasicActive = false;
int t = 0;

//double PI = 3.14159265

void setup() {
  //sets size of window
  size(WINDOW_SIZE,WINDOW_SIZE);
  //starts serial. My arduino uno is on COM3, change if needed
  udp= new UDP(this, PORT_RX, HOST_IP);
  udp.setTimeToLive(32);
  //udp.log(true);
  println("Is TX mulitcast: "+udp.isMulticast());
  println("Is TX Joined: "+udp.isJoined());
  
  
}

int yCommand= yPos;
int pCommand= pPos;
int rCommand= rPos;

void draw() {
  //make the background a sexy grey
  background(102);
  stroke(108);
  //creates frequency legend in the left side
  for(int i = 0; i < height/10; i++) { 
    line(0,height/100.0*i,50,height/100.0*i);
    textSize(10);
    fill(130, 130, 130);
    textAlign(LEFT);
    text(height-int(float(height)/100.0*i),2,height/100.0*i);
  } 
  

  
  //if (mousePressed)
  {
    if (rollActive)
    {
     rPos= (int(map(mouseY,0,WINDOW_SIZE,ROLL_MIN,ROLL_MAX)));
    }
    else if (pitchActive)
    {
      pPos = (int(map(mouseY,0,WINDOW_SIZE,PITCH_MIN,PITCH_MAX)));
    }
    else if (yawBasicActive)
    {
      yPos= (int(map(mouseY,0,WINDOW_SIZE,YAW_MIN,YAW_MAX)));
    }
    else
    {
      yCommand = yPos= (int(map(mouseY,0,WINDOW_SIZE,YAW_MIN,YAW_MAX)));
    }
    
    float yawRad = PI / 180.0 * .09 * (yPos-YAW_CENTER);
    
    //we want to "mix" pitch and roll such that pitch is 100% at yaw = 0, and roll is 100% at yaw = 90
    int rDelta = rPos - ROLL_CENTER;
    int pDelta = pPos - PITCH_CENTER;
    
    
    pCommand = int(PITCH_CENTER + (cos(yawRad) * pDelta) - (sin(yawRad) * rDelta));
    rCommand = int(ROLL_CENTER - (sin(yawRad) * pDelta) - (cos(yawRad) * rDelta));    
  }
  //creates frequency readout in the middle of the screen
  textSize(70);
  textAlign(CENTER);
  text(yCommand + "," + pCommand+ "," + rCommand, width/2, height/2); 
  
  textSize(10);
  textAlign(CENTER);


  int f = 0; 
  String s="{\"sound\": {\"vol\": 10, \"freq\": " + f + ", \"mode\": "+ 0 +"},"+
            " \"head\": {\"yaw\": " + yCommand+", \"roll\": " + rCommand + ", \"pitch\": " + pCommand + "}," +
            " \"body\": {\"r\": " + 255 + ", \"g\": " + 0 + ", \"b\": " + 0 + ", \"i\": " + 0 + "}}";
  
  udp.send(s,"192.168.42.1",PORT_RX);
// 
//  myPort.write(0xFE);
//  myPort.write(byte((int)(255.0)));
//  myPort.write(byte((int)(rgb[0]*255.0)));
//  myPort.write(byte((int)(rgb[1]*255.0)));
//  myPort.write(byte((int)(rgb[2]*255.0)));
//  myPort.write(byte(left));
//  myPort.write(byte(right));
//  
//  myPort.write(0x00);
//  myPort.write(0x00);
//  
//  myPort.write(0x00);
//  myPort.write(0x00);
//  
//  myPort.write(0x00);
//  myPort.write(0x00);
//  
//  myPort.write(char(xPos));
//  myPort.write(char(yPos&255));
//  myPort.write(char((yPos&(255<<8))>>8));
//
//  myPort.write(char(mouseClick));
//  myPort.write(0xFF);
 
}


void keyPressed() {
  if (key == 'r') 
  {
    rollActive = true;
    yawActive = false;
    pitchActive = false;
    yawBasicActive = false;
  }
  if (key == 'p') 
  {
    rollActive = false;
    yawActive = false;
    pitchActive = true;
    yawBasicActive = false;
  }
  if (key == 'y') 
  {
    rollActive = false;
    yawActive = true;
    pitchActive = false;
    yawBasicActive = false;
  }
  if (key == 'b') 
  {
    rollActive = false;
    yawActive = false;
    pitchActive = false;
    yawBasicActive = true;
  }
  println("Roll: " + rollActive + " Pitch:" + pitchActive + " Yaw:" + yawActive);
}

void keyReleased(){
  keyboardState = false;
  println();
}

void mouseReleased(){
 if (keyPressed == true) {
   if (noteState==true){
     noteState = false;
   }else{
     noteState = true;
   }
    
 }
}
