import hypermedia.net.*;

/**
Just a visualizer for Mira ;) 
*/

UDP udp;  // the UDP object
JSONObject json;

void setup() {
  size (1024,768, P3D);
   // create a multicast connection
  udp = new UDP( this, 41150, "224.1.1.2" );
  // wait constantly for incomming data
  udp.listen( true );
  // ... well, just verifies if it's really a multicast socket and blablabla
  println( "init as multicast socket ... "+udp.isMulticast() );
  println( "joins a multicast group  ... "+udp.isJoined() );
}

int camX = 0;
int camY = 100;
void draw() 
{
  lights();
  background(0);
  
  if (mousePressed)
  {
    camX = mouseX - width/2; 
    camY = mouseY - height/2;
  }
 //x right, y forward, z up
   camera(camX, 250, camY, // eyeX, eyeY, eyeZ
         0.0, 0.0, 0.0, // centerX, centerY, centerZ
         0.0, 0.0, -1.0); // upX, upY, upZ
  
  noStroke();

  //draw head
  float rHead = 50;
  pushMatrix();
  
  float eyeX = 10;
  float eyeZ = 25;
  float eyeY = sqrt((rHead*rHead) - (eyeZ * eyeZ) - (eyeX-eyeX));
  fill(255); 
  sphere(rHead);
  fill(0);
  pushMatrix();
  translate(eyeX, eyeY, eyeZ);
  sphere(5);
  popMatrix();
  pushMatrix();
  translate(-eyeX, eyeY, eyeZ);
  sphere(5);
  popMatrix();
  
  popMatrix();
  
  
  stroke( color(255, 0, 0));
  line(-100, 0, 0, 100, 0, 0);

  stroke( color(0, 255, 0));
  line(0, -100, 0, 0, 100, 0);
  
  stroke( color(0, 0, 255));
  line(0, 0, -100, 0, 0, 100);  
  
}

/**
 * This is the program receive handler. To perform any action on datagram 
 * reception, you need to implement this method in your code. She will be 
 * automatically called by the UDP object each time he receive a nonnull 
 * message.
 */
void receive( byte[] data ) {
  String json = new String(data);
  
  //{"sound": {"vol": 10, "freq": 0, "mode": 0}, "head": {"yaw": 1936, "roll": 1500, "pitch": 1075}}
  String[] my = match(json, "{\"yaw\": (.*?),");
  
  String[] mr = match(json, "{\"roll\": (.*?),");
  
  String[] mp = match(json, "{\"pitch\": (.*?),");
  
  println("test " + my[1] + " " + mp[1] + " " + mr[1]);
  
  
  
}
