import hypermedia.net.*;

/*
This code sends MouseY position and mouse click events though serial. It also draws scrolling history to be able to visualise Mira speak
into frequencies. The xPosition of the mouse controls volume. The yPosition of the mouse controls pitch. Mouse clicks make sounds. Any keyboard
press + mouse click will go into recording mode and print out code required to recreate sounds. 
*/

import processing.serial.*;
Serial myPort; 
UDP udp;
int PORT_RX=41150;
String HOST_IP = "224.1.1.2";//IP Address of the PC in which this App is running

int screenSize = 500;
int xPos = 0; 
int yPos = 0; 

int YAW_MIN= 1000;
int YAW_MAX = 2000;
int YAW_CENTER = 1580;

int PITCH_MIN = 1000;
int PITCH_MAX = 1600;
int PITCH_CENTER = 1400;

int ROLL_MIN = 1000;
int ROLL_MAX = 2000;
int ROLL_CENTER = 1575;


int yCommand= YAW_CENTER;
int pCommand= PITCH_CENTER;
int rCommand= ROLL_CENTER;

//An array of y values
int[] xvals;
int[] yvals;
String inString;  // Input string from serial port

byte mouseClick =0;

int oldTime;
int newTime;
int oldFreq;
int newFreq;
boolean noteState = true;
boolean keyboardState = false;

int t = 0;

int autotune (int x)
{       
  if( x<=  32  ) x=  31  ;
  if( x >  32  &&  x<=  34  ) x=  33  ;
  if( x >  34  &&  x<=  36  ) x=  35  ;
  if( x >  36  &&  x<=  38  ) x=  37  ;
  if( x >  38  &&  x<=  40  ) x=  39  ;
  if( x >  40  &&  x<=  42.5  ) x=  41  ;
  if( x >  42.5  &&  x<=  45  ) x=  44  ;
  if( x >  45  &&  x<=  47.5  ) x=  46  ;
  if( x >  47.5  &&  x<=  50.5  ) x=  49  ;
  if( x >  50.5  &&  x<=  53.5  ) x=  52  ;
  if( x >  53.5  &&  x<=  56.5  ) x=  55  ;
  if( x >  56.5  &&  x<=  60  ) x=  58  ;
  if( x >  60  &&  x<=  63.5  ) x=  62  ;
  if( x >  63.5  &&  x<=  67  ) x=  65  ;
  if( x >  67  &&  x<=  71  ) x=  69  ;
  if( x >  71  &&  x<=  75.5  ) x=  73  ;
  if( x >  75.5  &&  x<=  80  ) x=  78  ;
  if( x >  80  &&  x<=  84.5  ) x=  82  ;
  if( x >  84.5  &&  x<=  90  ) x=  87  ;
  if( x >  90  &&  x<=  95.5  ) x=  93  ;
  if( x >  95.5  &&  x<=  101  ) x=  98  ;
  if( x >  101  &&  x<=  107  ) x=  104  ;
  if( x >  107  &&  x<=  113.5  ) x=  110  ;
  if( x >  113.5  &&  x<=  120  ) x=  117  ;
  if( x >  120  &&  x<=  127  ) x=  123  ;
  if( x >  127  &&  x<=  135  ) x=  131  ;
  if( x >  135  &&  x<=  143  ) x=  139  ;
  if( x >  143  &&  x<=  151.5  ) x=  147  ;
  if( x >  151.5  &&  x<=  160.5  ) x=  156  ;
  if( x >  160.5  &&  x<=  170  ) x=  165  ;
  if( x >  170  &&  x<=  180  ) x=  175  ;
  if( x >  180  &&  x<=  190.5  ) x=  185  ;
  if( x >  190.5  &&  x<=  202  ) x=  196  ;
  if( x >  202  &&  x<=  214  ) x=  208  ;
  if( x >  214  &&  x<=  226.5  ) x=  220  ;
  if( x >  226.5  &&  x<=  240  ) x=  233  ;
  if( x >  240  &&  x<=  254.5  ) x=  247  ;
  if( x >  254.5  &&  x<=  269.5  ) x=  262  ;
  if( x >  269.5  &&  x<=  285.5  ) x=  277  ;
  if( x >  285.5  &&  x<=  302.5  ) x=  294  ;
  if( x >  302.5  &&  x<=  320.5  ) x=  311  ;
  if( x >  320.5  &&  x<=  339.5  ) x=  330  ;
  if( x >  339.5  &&  x<=  359.5  ) x=  349  ;
  if( x >  359.5  &&  x<=  381  ) x=  370  ;
  if( x >  381  &&  x<=  403.5  ) x=  392  ;
  if( x >  403.5  &&  x<=  427.5  ) x=  415  ;
  if( x >  427.5  &&  x<=  453  ) x=  440  ;
  if( x >  453  &&  x<=  480  ) x=  466  ;
  if( x >  480  &&  x<=  508.5  ) x=  494  ;
  if( x >  508.5  &&  x<=  538.5  ) x=  523  ;
  if( x >  538.5  &&  x<=  570.5  ) x=  554  ;
  if( x >  570.5  &&  x<=  604.5  ) x=  587  ;
  if( x >  604.5  &&  x<=  640.5  ) x=  622  ;
  if( x >  640.5  &&  x<=  678.5  ) x=  659  ;
  if( x >  678.5  &&  x<=  719  ) x=  698  ;
  if( x >  719  &&  x<=  762  ) x=  740  ;
  if( x >  762  &&  x<=  807.5  ) x=  784  ;
  if( x >  807.5  &&  x<=  855.5  ) x=  831  ;
  if( x >  855.5  &&  x<=  906  ) x=  880  ;
  if( x >  906  &&  x<=  960  ) x=  932  ;
  if( x >  960  &&  x<=  1017.5  ) x=  988  ;
  if( x >  1017.5  &&  x<=  1078  ) x=  1047  ;
  if( x >  1078  &&  x<=  1142  ) x=  1109  ;
  if( x >  1142  &&  x<=  1210  ) x=  1175  ;
  if( x >  1210  &&  x<=  1282  ) x=  1245  ;
  if( x >  1282  &&  x<=  1358  ) x=  1319  ;
  if( x >  1358  &&  x<=  1438.5  ) x=  1397  ;
  if( x >  1438.5  &&  x<=  1524  ) x=  1480  ;
  if( x >  1524  &&  x<=  1614.5  ) x=  1568  ;
  if( x >  1614.5  &&  x<=  1710.5  ) x=  1661  ;
  if( x >  1710.5  &&  x<=  1812.5  ) x=  1760  ;
  if( x >  1812.5  &&  x<=  1920.5  ) x=  1865  ;
  if( x >  1920.5  &&  x<=  2034.5  ) x=  1976  ;
  if( x >  2034.5  &&  x<=  2155  ) x=  2093  ;
  if( x >  2155  &&  x<=  2283  ) x=  2217  ;
  if( x >  2283  &&  x<=  2419  ) x=  2349  ;
  if( x >  2419  &&  x<=  2563  ) x=  2489  ;
  if( x >  2563  &&  x<=  2715.5  ) x=  2637  ;
  if( x >  2715.5  &&  x<=  2877  ) x=  2794  ;
  if( x >  2877  &&  x<=  3048  ) x=  2960  ;
  if( x >  3048  &&  x<=  3229  ) x=  3136  ;
  if( x >  3229  &&  x<=  3421  ) x=  3322  ;
  if( x >  3421  &&  x<=  3624.5  ) x=  3520  ;
  if( x >  3624.5  &&  x<=  3840  ) x=  3729  ;
  if( x >  3840  &&  x<=  4068.5  ) x=  3951  ;
  if( x >  4068.5  &&  x<=  4310.5  ) x=  4186  ;
  if( x >  4310.5  &&  x<=  4567  ) x=  4435  ;
  if( x >  4567  &&  x<=  4838.5  ) x=  4699  ;
  if( x >  4838.5) x=  4978  ;
  return x;
}

//function to converts HSV to RGB color space. 
void HSVtoRGB( float[] rgb, float h, float s, float v )
{
  int i;
  float f, p, q, t;
  if( s == 0 ) {
    // achromatic (grey)
    rgb[0] = rgb[1] = rgb[2] = (int)v;
    return;
  }
  
  h *= 6;      // sector 0 to 5
  i = floor( h );
  f = h - i;    // factorial part of h
  p = v * ( 1 - s );
  q = v * ( 1 - s * f );
  t = v * ( 1 - s * ( 1 - f ) );
  switch( i ) {
    case 0:
      rgb[0] = v; rgb[1]  =t; rgb[2] = p;break;
    case 1:
      rgb[0] = q; rgb[1]  = v; rgb[2] = p;break;
    case 2:
      rgb[0] = p; rgb[1]  = v; rgb[2] = t;break;
    case 3:
      rgb[0] = p; rgb[1]  = q; rgb[2] = v;break;
    case 4:
      rgb[0] = t; rgb[1]  = p; rgb[2] = v;break;
    default:    
      rgb[0] = v; rgb[1] = p; rgb[2] = q;break;
  }
}

void setup() {
  //sets size of window
  size(screenSize,screenSize);
  //starts serial. My arduino uno is on COM3, change if needed
  udp= new UDP(this, PORT_RX, HOST_IP);
  udp.setTimeToLive(32);
  //udp.log(true);
  println("Is TX mulitcast: "+udp.isMulticast());
  println("Is TX Joined: "+udp.isJoined());
  
  //creates an array with the length of the height of the window. One per pixel
  xvals = new int[height];
  yvals = new int[height];
  for(int i = 1; i < width; i++) { 
    //populates all array values with width of window
    xvals[i-1] = width;
    yvals[i-1] = width;
  } 
  
//  float[] rgb = new float[3];
//  HSVtoRGB(rgb,0.5, 1.0, 1.0 );
//  for (int i=0; i<3; i++)
//     println(rgb[i]);
//    
}


void draw() {
  //make the background a sexy grey
  background(102);
  stroke(108);
  //creates frequency legend in the left side
  for(int i = 0; i < height/10; i++) { 
    line(0,height/100.0*i,50,height/100.0*i);
    textSize(10);
    fill(130, 130, 130);
    textAlign(LEFT);
    text(height-int(float(height)/100.0*i),2,height/100.0*i);
  } 
  //range of volume needs to be 0 to 10 for toneAC library
  xPos= constrain(int(map(mouseX,0,screenSize,1,11)),0,10);
  //invert y position of mouse through a remap
  yPos= constrain(int(map(mouseY,0,screenSize,2000,0)),0,2000);
  //yPos = autotune(yPos);
      
  //this advances time for the scrolling background
  for(int i = 1; i < width; i++) { 
    xvals[i-1] = xvals[i];
    yvals[i-1] = yvals[i];
  } 
  // Add the new values to the end of the array 
  xvals[width-1] = constrain(mouseX,0,width);
  yvals[width-1] = constrain(mouseY,0,height);
  
  //detects mouse clicks
  if (mousePressed == true) {
    mouseClick = 1;
  } else{
    mouseClick = 0;
    //if mouse not clicked, make value recorded the highest value
    xvals[width-1] = 0;
  }

  //draws the points for scrolling background 
  for(int i=1; i<width; i++) {
    stroke(180);
    stroke(map(xvals[i],0,screenSize,102,255));
    point(i, yvals[i]);
  }
 
  //This is the code that prints out when recording
  
  if (keyboardState == false && keyPressed){
    println("<<<<<<<<<<<<<<<<<<< RECORDING >>>>>>>>>>>>>>>>>>>>");
    keyboardState = true;
    oldTime = newTime;
    newTime = millis();
  }
  
  if (keyPressed) {
    oldFreq = newFreq;
    newFreq = yPos;
    fill(120, 80, 80);
    if (mousePressed == true) {
      if (newFreq >= oldFreq && newFreq <= oldFreq && noteState == true){
        oldTime = newTime;
        newTime = millis();
          print((newTime-oldTime)+"), ");
          print("Word(" + yPos +","+ xPos+",");
        noteState = false;
      }else if (newFreq < oldFreq || newFreq > oldFreq) {
          oldTime = newTime;
          newTime = millis();
          print((newTime-oldTime)+"), ");
          print("Word(" + yPos +","+ xPos+",");
          noteState = true;
      }
    }
  }else{
    fill(130, 130, 130); 
  }
    
  float[] rgb = new float[3];
  HSVtoRGB(rgb, constrain(yPos/2000.0,0.0,1.0), 1.0, 1.0 );
  for (int i=0; i<3; i++)
    rgb[i] = rgb[i] * 255.0;
  //creates frequency readout in the middle of the screen
  textSize(70);
  textAlign(CENTER);
  text(yPos, width/2, height/2);
 
  text("r:" + (int)rgb[0] + " g:" + (int)rgb[1] + " b:" + (int)rgb[2], width/2, height/2+70);  
  textSize(10);
  textAlign(CENTER);
  text("received: " + inString,  width/2,50); 
  //writes startbyte,mouseY position broken up into two bytes, and mouse click into serial

  
  
  String s="{\"sound\": {\"vol\": 10, \"freq\": " + yPos + ", \"mode\": "+mouseClick+"},"+
            " \"head\": {\"yaw\": " + yCommand+", \"roll\": " + rCommand + ", \"pitch\": " + pCommand + "}," +
            " \"body\": {\"r\": " + (int)rgb[0] + ", \"g\": " + (int)rgb[1] + ", \"b\": " + (int)rgb[2] + ", \"i\": " + 0 + "}}";
  
  udp.send(s,"192.168.42.1",PORT_RX);
// 
//  myPort.write(0xFE);
//  myPort.write(byte((int)(255.0)));
//  myPort.write(byte((int)(rgb[0]*255.0)));
//  myPort.write(byte((int)(rgb[1]*255.0)));
//  myPort.write(byte((int)(rgb[2]*255.0)));
//  myPort.write(byte(left));
//  myPort.write(byte(right));
//  
//  myPort.write(0x00);
//  myPort.write(0x00);
//  
//  myPort.write(0x00);
//  myPort.write(0x00);
//  
//  myPort.write(0x00);
//  myPort.write(0x00);
//  
//  myPort.write(char(xPos));
//  myPort.write(char(yPos&255));
//  myPort.write(char((yPos&(255<<8))>>8));
//
//  myPort.write(char(mouseClick));
//  myPort.write(0xFF);
 
}

void serialEvent(Serial p) { 
  inString = p.readString(); 
} 

void keyReleased(){
  keyboardState = false;
  println();
}
void mouseReleased(){
 if (keyPressed == true) {
   if (noteState==true){
     noteState = false;
   }else{
     noteState = true;
   }
    oldTime = newTime;
    newTime = millis();
    print((newTime-oldTime)+"), ");
    newFreq = 0;
    print("Word(" + newFreq +","+ xPos+",");
 }
}
