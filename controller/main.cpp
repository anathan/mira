#include <iostream>
#include <fstream>
 
#include <boost/regex.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>

#include "controlManager.h"

#include "../common/rapidjson/rapidjson.h"
#include "../common/rapidjson/stringbuffer.h"
#include "../common/rapidjson/writer.h"
#include "../common/rapidjson/prettywriter.h"
#include "../common/network/sender.h"
#include "../common/network/receiver.h"
#include "../common/network/endpoints.h"

using namespace std;

//Take control messages off the UDP network and shove them over the serial port
int main(int argc, char** argv)
{
    boost::asio::io_service io_service;      
    ControlManager controlManager(io_service);    
    io_service.run();

    return 0;
}
