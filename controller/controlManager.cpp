#include "controlManager.h"

#include "../common/network/endpoints.h"


void ControlManager::onControlMessage(std::string s)
{
    //std::cout << "CTRL UDP RX: " << s << std::endl;
    //the control message should be json
    if (!fromJson(cp, s))
        std::cout << "Warning invalid JSON received: " << s << std::endl;
}

void ControlManager::handle_timeout(boost::system::error_code const& cError)
{
    if (cError.value() == boost::asio::error::operation_aborted)
        return;

    if (cError && cError.value() != boost::asio::error::operation_aborted)
    {
        std::cout << "Warning Serial Timer shit the bed." << cError << std::endl;
        return; // throw an exception?
    }

    if (serial != NULL)
       serial->write(&cp, sizeof(cp));
   
    ////change stuff
    ////std::cout << "CTRL UDP TX "<< sizeof(cp) << std::endl;
    //cp.startByte = 0xFE;
    //int x= cp.rBody+1;
    //cp.rBody=x;
    //cp.gBody=x;
    //cp.bBody=x;
    //cp.iBody=x;
    //cp.ledLeftEye=x;
    //cp.ledRightEye=x;
    //cp.yawPos=x;   //YAW_CENTER;
    //cp.pitchPos=x; //PITCH_CENTER;
    //cp.rollPos=x;  //ROLL_CENTER;
    //cp.soundFreq=x;
    //cp.soundMode=x;
    //cp.soundVol=x;
    //cp.checksumByte = 0;

    // Schedule the timer again...
    serailMsgTimer.expires_from_now(boost::posix_time::milliseconds(1000 / SERIAL_LOOP_HZ)); // runs every 1/SERIAL_LOOP ms
    serailMsgTimer.async_wait(boost::bind(&ControlManager::handle_timeout, this, boost::asio::placeholders::error));
}



void ControlManager::onSerialRx(const char *data, unsigned int len)
{
    for (unsigned int i = 0; i < len; i++)
    {
        incomingSerialMsg.append(data + i, 1);
        if (data[i] == '\n')
        {
            cout << "SERIAL RX: " << incomingSerialMsg << endl;
            incomingSerialMsg = "";
        }
    }        
}

ControlManager::ControlManager(boost::asio::io_service& io_service) :
    serailMsgTimer(io_service)
{
    serial = NULL;
    try
    {
#ifdef _MSC_VER
        serial = new ControlSerialInterface(io_service, 115200, "COM3");
        std::cout<<"Using COM3 for Serial Control"<<std::endl;
#else
        serial = new ControlSerialInterface(io_service, 115200, "/dev/ttyAMA0");
        std::cout << "Using /dev/ttyAMA0 for Serial Control" << std::endl;
#endif     
    }
    catch (const boost::system::system_error& e)
    {
        cerr << endl << "Serial Port Could not be opened likely becuase it doesnt exist. Original exception:" << e.what() << "...";
        cerr << "Program will continue without sending serial messages." << endl;
    }

    cp.startByte = 0xFE;
    cp.rBody=0;
    cp.gBody=0;
    cp.bBody=0;
    cp.iBody=0;
    cp.ledLeftEye=0;
    cp.ledRightEye=0;
    cp.yawPos=YAW_CENTER;
    cp.pitchPos=PITCH_CENTER;
    cp.rollPos=ROLL_CENTER;
    cp.soundFreq=0;
    cp.soundMode=0;
    cp.soundVol=0;
    cp.checksumByte=0;

    receiver = new Receiver(io_service, boost::asio::ip::address::from_string(LISTEN_IP_AUTO), boost::asio::ip::address::from_string(CONTROLLER_IP), CONTROLLER_PORT);
    receiver->register_callback(boost::bind(&ControlManager::onControlMessage, this, _1));
    
   
    serailMsgTimer.expires_from_now(boost::posix_time::milliseconds((1000 / SERIAL_LOOP_HZ)));
    serailMsgTimer.async_wait(boost::bind(&ControlManager::handle_timeout, this, boost::asio::placeholders::error));
    
}

ControlManager::~ControlManager()
{
    if (serial != NULL)
    {
        serial->close();
        delete serial;
    }
    if (receiver != NULL)            
        delete receiver;    
}
