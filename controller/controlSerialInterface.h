#include <deque> 
#include <boost/bind.hpp> 
#include <boost/asio.hpp> 
#include <boost/asio/serial_port.hpp> 
#include <boost/thread.hpp> 
#include <boost/lexical_cast.hpp> 
#include <boost/date_time/posix_time/posix_time_types.hpp> 

#ifdef POSIX 
#include <termios.h> 
#endif 

using namespace std;

class ControlSerialInterface
{
public:
    ControlSerialInterface(boost::asio::io_service& io_service, unsigned int baud, const string& device);
    void write(uint8_t msg); // pass the write data to the do_write function via the io service in the other thread     
    void write(void* d, size_t size);
    void close();
    bool active(); // return true if the socket is still active 
    
private:

    static const int max_read_length = 512; // maximum amount of data to read in one operation 

    void read_start(void);
    void read_complete(const boost::system::error_code& error, size_t bytes_transferred);
    void do_write(uint8_t msg);
    void write_start(void);
    void write_complete(const boost::system::error_code& error);
    void do_close(const boost::system::error_code& error);

    bool active_; // remains true while this object is still operating 
    boost::asio::io_service& io_service_; // the main IO service that runs this connection 
    boost::asio::serial_port serialPort; // the serial port this instance is connected to 
    char read_msg_[max_read_length]; // data read from the socket 
    deque<uint8_t> write_msgs_; // buffered write data 
};