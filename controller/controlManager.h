#ifndef CONTROL_SENDER_H_
#define CONTROL_SENDER_H_
#include <iostream>
#include <fstream>
#include <string>

#include "controlSerialInterface.h"

#include "../common/network/receiver.h"
#include "../common/interfaces/controller.h"

#define SERIAL_LOOP_HZ  40

//Ties together the UDP multicast json blobs for control into a single, cohesive
//message to be sent over serial
class ControlManager
{
public:

    ControlManager(boost::asio::io_service& io_service);
    ~ControlManager();
private:
    void onControlMessage(std::string s);
    void onSerialRx(const char *data, unsigned int len);
    void handle_timeout(boost::system::error_code const& cError);
    ControlSerialInterface* serial;
    CommandPacket cp;
    std::string incomingSerialMsg;
    boost::asio::deadline_timer serailMsgTimer;
    Receiver* receiver;
};

#endif
