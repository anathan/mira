#!/bin/sh
 # launcher.sh
 # navigate to home directory, then to this directory, then execute python script, then back home

cd /
cd home/pi/mira/controller
sudo ./controller &
cd /home/pi/mira/facedetect
sudo ./facedetect --gui false &
cd /
