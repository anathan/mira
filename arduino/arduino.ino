//MIRA Arduino Program

//NOTE: this requires the Teensy extensions and all libraries to compile properly.
// Download it here: https://www.pjrc.com/teensy/td_download.html


//Incredible change happens in your life when you decide to take
//control of what you do have power over instead of craving control 
//over what you don't

#include <Adafruit_NeoPixel.h>
#include "ServoMira.h"
#include "pitches.h"

#define NEOPIXELPIN  2
#define SOUND_PIN_1  12
//#define SOUND_PIN_2  10

#define YAW_PIN   5
#define PITCH_PIN 6
#define ROLL_PIN  9

Servo yawServo;
Servo pitchServo;
Servo rollServo;
#define PWM_FREQ 100

int LED_PIN = 13;
int YAW_ID = 0;
int PITCH_ID = 1;
int ROLL_ID = 2;

int YAW_MIN = 1000;
int YAW_CENTER = 1600;
int YAW_MAX = 2000;

int PITCH_MIN = 1000;
int PITCH_CENTER = 1450;
int PITCH_MAX = 1600;

int ROLL_MIN = 1170;
int ROLL_CENTER = 1600;
int ROLL_MAX = 2000;
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(1, NEOPIXELPIN, NEO_GRB + NEO_KHZ800);

uint8_t START_BYTE = 0xFE;

#pragma pack(1)
typedef struct {
  uint8_t startByte;
  uint8_t iBody;
  uint8_t rBody;
  uint8_t gBody;
  uint8_t bBody;
  uint8_t ledLeftEye;
  uint8_t ledRightEye;
  uint16_t yawPos;
  uint16_t pitchPos;
  uint16_t rollPos;
  uint8_t  soundVol;
  uint16_t soundFreq;
  uint8_t  soundMode;
  uint8_t  checksumByte;
} CommandPacket;
#pragma pack()

int flash = 0;
volatile int incomingPacketPosition = 0;
CommandPacket incomingPacket;
CommandPacket currentPacket;
char serialOutput[100];


//motion control vars
float filteredYaw = YAW_CENTER;
float filteredPitch = PITCH_CENTER;
float filteredRoll = ROLL_CENTER;
unsigned long MIN_CONTROL_DT_MS = 5; 

float gainYaw = .01f;
float gainPitch = .01f;
float gainRoll = .01f;
 
float minYawEffort = 1;
float minPitchEffort = 1;
float minRollEffort = 1;

unsigned long lastControlRun = 0;


//// notes in the melody:
//int melody[] = {
//  //c4 c4 c8 c4 ds4 ds8 d4 c8 c4 b8 c1 
//  NOTE_C4, 
//  NOTE_C4,
//  NOTE_C4,
//  NOTE_C4,
//  NOTE_DS4,
//  NOTE_DS4,
//  NOTE_D4,
//  NOTE_C4,
//  NOTE_C4,
//  NOTE_B3,
//  NOTE_C4,
//};

int melody[] = {
  //c4 c4 c8 c4 ds4 ds8 d4 c8 c4 b8 c1 
  NOTE_C4, 
  NOTE_D4,
  NOTE_E4,
  NOTE_F4,
  NOTE_G4,
  NOTE_A4,
  NOTE_B4,
  NOTE_C5
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  1, 2, 4, 1, 2, 4, 2, 4, 2, 4, 1
};
void setup()
{
  delay(3);
  currentPacket.yawPos = YAW_CENTER;
  currentPacket.pitchPos = PITCH_CENTER;
  currentPacket.rollPos = ROLL_CENTER;
 
  yawServo.attach(YAW_PIN);
  pitchServo.attach(PITCH_PIN);
  rollServo.attach(ROLL_PIN);

  yawServo.writeMicroseconds(YAW_CENTER);
  pitchServo.writeMicroseconds(PITCH_CENTER);
  rollServo.writeMicroseconds(ROLL_CENTER);
  delay(1000);
  pixels.begin();
//  for (int thisNote = 0; thisNote < 11; thisNote++) 
//  {
//    int noteDuration = 1000/noteDurations[thisNote];
//    tone(SOUND_PIN_1,melody[thisNote], noteDuration); // Play thisNote at full volume for noteDuration in the background.   
//    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
//    pixels.setPixelColor(0, pixels.Color(0,(int)(255.0 * (thisNote/12.0)),0)); 
//    pixels.show(); // This sends the updated pixel color to the hardware.
//
//    delay(noteDuration * 4 / 3); // Wait while the tone plays in the background, plus another 33% delay between notes.   
//    
//  }

  for (int i=0; i<8000; i++)
  {
    if (i%1000 == 0)
      tone(SOUND_PIN_1,melody[i/1000], 100);
    if (i>0 && i < 8000/3)
      pixels.setPixelColor(0, pixels.Color((int)(255.0 * (i/8000.0)),0,0));
    else if (i>8000/3 && i < (8000/3 * 2))
      pixels.setPixelColor(0, pixels.Color(0,(int)(255.0 * ((i-8000/3)/8000.0)),0));
    else if (i>(8000/3 * 2))
        pixels.setPixelColor(0, pixels.Color(0,0,(int)(255.0 * ((i-8000/3*2)/8000.0))));
     delayMicroseconds(65);
     //pixels.show();
  }
  
  Serial1.begin(115200, SERIAL_8N1);
  Serial1.clear();
}

uint16_t lastFreq= 0;
void loop()
{
  
  if (flash >= 10000)
  {
    
    
    sprintf(serialOutput, "%d %d %d %d %d %d %d %d %d %d %d %d %d", 
      currentPacket.iBody, 
      currentPacket.rBody, 
      currentPacket.gBody, 
      currentPacket.bBody,
      currentPacket.ledLeftEye,
      currentPacket.ledRightEye,
      currentPacket.yawPos,
      currentPacket.pitchPos,
      currentPacket.rollPos,
      currentPacket.soundVol,
      currentPacket.soundFreq,
      currentPacket.soundMode,
      sizeof(CommandPacket));
    Serial1.println(serialOutput);
     /*
    uint8_t* rawPacket = (uint8_t*)(&currentPacket);  
    for (int i=0; i<sizeof(CommandPacket); i++)
    {
       Serial1.print(rawPacket[i], HEX);
       Serial1.print(' ');
    }
    Serial1.println();
    */
    flash = 0;
  }
  flash++;


  //the current command should be in the currentPacket struct.
  //we're guaranteed that will be atomically consistent

  //control logic
  //the concept here is to only make small changes in the direction of the 
  //targeted position, using a proportional constant of the error
  if ( millis() - lastControlRun > MIN_CONTROL_DT_MS)
  {
    float yError = currentPacket.yawPos - filteredYaw;
    float pError = currentPacket.pitchPos - filteredPitch;
    float rError = currentPacket.rollPos - filteredRoll;
    
    float yEffort = yError * gainYaw;
    float pEffort = pError * gainPitch;
    float rEffort = rError * gainRoll;

    //clamp minimum efforts (preserving sign)
    if (yEffort > 0 && yEffort < minYawEffort) yEffort = minYawEffort;
    if (yEffort < 0 && yEffort > -minYawEffort) yEffort = -minYawEffort;
    if (pEffort > 0 && pEffort < minPitchEffort) pEffort = minPitchEffort;
    if (pEffort < 0 && pEffort > -minPitchEffort) pEffort = -minPitchEffort;
    if (rEffort > 0 && rEffort < minRollEffort) rEffort = minRollEffort;
    if (rEffort < 0 && rEffort > -minRollEffort) rEffort = -minRollEffort;
    
    filteredYaw +=   (yEffort);
    filteredPitch += (pEffort);
    filteredRoll +=  (rEffort);
    //NOTE: pixel.show is actually called by the servo libarry now on the falling edge
    
   /* yawServo.writeMicroseconds(constrain(filteredYaw, YAW_MIN, YAW_MAX));
    pitchServo.writeMicroseconds(constrain(filteredPitch, PITCH_MIN, PITCH_MAX));
    rollServo.writeMicroseconds(constrain(filteredRoll, ROLL_MIN, ROLL_MAX));
*/
    yawServo.writeMicroseconds(constrain(currentPacket.yawPos, YAW_MIN, YAW_MAX));
    pitchServo.writeMicroseconds(constrain(currentPacket.pitchPos, PITCH_MIN, PITCH_MAX));
    rollServo.writeMicroseconds(constrain(currentPacket.rollPos, ROLL_MIN, ROLL_MAX));

                
    //light & sound
    pixels.setPixelColor(0,pixels.Color(currentPacket.rBody, currentPacket.gBody, currentPacket.bBody));
    if (currentPacket.soundMode == 0)
    {
      //silence
      noTone(SOUND_PIN_1);
      lastFreq=0;
    }
    else
    {     
      //humans hear sound change in a logrithmic way, this makes it linear increments in how we hear sound.
      //float yPos = 40 * pow(2.0, currentPacket.soundFreq/108.0);
      if (currentPacket.soundFreq != lastFreq)
      {
        tone(SOUND_PIN_1, currentPacket.soundFreq);
        lastFreq = currentPacket.soundFreq;
      }
    }
    
   
    
    lastControlRun = millis();
  }
  
}

void serialEvent1() {

  uint8_t* rawPacket = (uint8_t*)(&incomingPacket);
  while (Serial1.available()) {
    int b = Serial1.read(); // get the new byte
    if (b== -1)
      return;
    if (incomingPacketPosition ==0 && b != START_BYTE) //wait for the start byte
      continue;
    rawPacket[incomingPacketPosition] = (uint8_t)b;
    incomingPacketPosition++;

    if (incomingPacketPosition == sizeof(CommandPacket)) {
      currentPacket = incomingPacket;
      incomingPacketPosition = 0;
    }
  }
}





